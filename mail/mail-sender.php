<?php

require_once("../wp-load.php");
require_once('../wp-includes/pluggable.php');

$site_url = WP_ENV === 'development' ? 'http://mrport.test' : 'https://mrportservice.se/';

/****** INSTALLATION ******/
// Lägg denna fil samt bilden mail_social_sprite.png i rooten i din WordPress installation
// Se även filen smtp_wp_mailer-instruktioner.txt

/****** SMTP-PLUGIN ******/
// WP-Mail-SMTP https://wordpress.org/plugins/wp-mail-smtp/

/****** CAPTCHA ******/
// REGISTER SITE AT https://www.google.com/recaptcha/
const RECAPTCHA_SCORE_LIMIT = 0.5;
// Add the recaptcha() -function to validation

// Content Type - Ändra ifall det inte ska vara HTML 
function emailContentType($contentType)
{
	return 'text/html';
}
add_filter('wp_mail_content_type', 'emailContentType');

// From - Ändra till epostadressen det ska stå att mejlet kommer ifrån
function emailFrom($from)
{
	return 'noreply@mrportservice.se';
}
add_filter('wp_mail_from', 'emailFrom');

// From Name - Ändra till namnet det ska stå att mejlet kommer ifrån
function emailFromName($from)
{
	return 'MR Portservice';
}
add_filter('wp_mail_from_name', 'emailFromName');

/************************************* GLOBALS *************************************/
$boundary = uniqid('np'); //BOUNDARY (DON'T TOUCH)

$url 		= get_field('mail_bas_url_till_site', 'epost');
$foretag 	= get_field('mail_foretagsnamn', 'epost');
$adress 	= get_field('mail_adress', 'epost');
$cmail 		= get_field('mail_kontakt_epost', 'epost');
$ctelefon 	= get_field('mail_kontakt_telefon', 'epost');
$cright 	= get_field('mail_copyright_text', 'epost');

$color 		= get_field('mail_huvudfarg', 'epost');
$tlogo 		= get_field('mail_logotyp_sidhuvud', 'epost');
$blogo 		= get_field('mail_logotyp_sidfot', 'epost');

$facebook 	= get_field('mail_facebook_url', 'epost');
$instagram 	= get_field('mail_instagram_url', 'epost');
$linkedin 	= get_field('mail_linkedin_url', 'epost');
$twitter 	= get_field('mail_twitter_url', 'epost');

/************************************* KONTAKFORMULÄR *************************************/
/*-------------------------------- Återfinns på sidorna: --------------------------------*/
/* /kontakt */



if(isset($_GET['contactForm'])){

	if ( 
		stristr($_SERVER['HTTP_REFERER'],$site_url) && 
		smtp_validate($_POST['footerName']) && 
		smtp_validate($_POST['footerEmail'],'footerEmail') && 
		smtp_validate($_POST['footerMessage']) && 
		smtp_validate($_POST['footerPhone']) && 
		!stristr($_POST['footerMessage'], 'http://') && 
		!stristr($_POST['footerMessage'], 'https://') && 
		recaptcha()
	) {

		$name 		= $_POST['footerName'];
		$phone 		= $_POST['footerPhone'];
		$email 		= $_POST['footerEmail'];
		$message 	= $_POST['footerMessage'];
		$fromUrl 	= $_POST['url'];

		//SET AN ID FOR FORM
		$form_id = 'Kontaktformulär';

		//TITLE IN MAIL
		$subject = "Kontaktformulär";

		//SHORT EXCERPT VIEWED IN EG. GMAIL
		$excerpt = 'Kontaktad via kontaktformuläret på MR Portservice';

		//MAIN MESSAGE IN MAIL
		$message = "<h1 style='font-size:24px;line-height:30px;margin:0 0 0;'>".$subject."</h1>
		<h2 style='font-size:18px;font-weight:400;line-height:30px;margin:0 0 20px;color:".$color.";'>".$excerpt."</h2>
		Namn: ".$name."<br />
		Telefon: ".$phone."<br />
		E-post: ".$email."<br />
		<br />
		Meddelande:<br />
		".nl2br($message)."<br />";

		$to = 'martin@teckentrupcenter.se';

		$headers = 'Content-Type: text/html; charset=UTF-8';


		wp_mail($to, 'Kontaktsida mrportservice.se', smtp_mail($subject, $message, $excerpt, $fromUrl, $form_id), $headers);
		
		header("Location:".$fromUrl."?success");
		exit;
	}else{
		header("Location:".$_POST['url']."?failed");
	 	exit;
	}
}


/************************************* FUNTIONER *************************************/
function recaptcha()
{
    if (!isset($_POST['g-recaptcha-response'])) {
        return false;
    }

    $captchaResponse = $_POST['g-recaptcha-response'];

    $verificationUrl = 'https://www.google.com/recaptcha/api/siteverify';
    $body = [
        'secret' => GRECAPTCHA_PRIVATE,
        'response' => $captchaResponse,
        'remoteip' => $_SERVER['REMOTE_ADDR']
    ];

    $response = wp_remote_post($verificationUrl, [
        'body' => $body,
    ]);

    $body = wp_remote_retrieve_body($response);
    $response = json_decode($body);

		if ($response->success === true) {
			if ($response->score >= RECAPTCHA_SCORE_LIMIT) {
				if ($response->action === basename($_POST['action'])) {
					if ($response->hostname === $_SERVER['SERVER_NAME']) {
						return true;
					}
				}
			}
		}

    return false;
}

function smtp_validate($validate,$field_type = null){
	if( !empty($validate) && searchStringForSpam($validate) && field_type_validate($validate,$field_type) ){
		return true;
	}else{
	 	return false;
	}
}

function searchStringForSpam($validate){
	$strings_to_search = array('href=', '[/url');
	$spam = false;
	foreach ($strings_to_search as $needle) {
		if( stristr($validate, $needle)  ){
			$spam = true;
			return false;
		}
	}

	if($spam){
		return false;
	}else{
		return true;
	}
}

function field_type_validate($validate,$field_type){
	if($field_type === 'email'){
		if (!filter_var($validate, FILTER_VALIDATE_EMAIL)) {
			return false;
		}else{
			return true;
		}
	}else{
		return true;
	}
}

function smtp_sub($sub){
	$subject = mb_encode_mimeheader(utf8_decode($sub), 'UTF-8', 'B');
	return $subject;
}

function smtp_params($from){
	$params = "-f ".$from;
	return $params;
}

function smtp_mail($sub,$message,$excerpt,$page_sender,$form_id) {
	global $url;
	global $foretag;
	global $color;
	global $tlogo;
	global $blogo;
	global $cmail;
	global $ctelefon;
	global $adress;
	global $facebook;
	global $instagram;
	global $linkedin;
	global $twitter;

	if($facebook){
		$facebook = '<a href="'.$facebook.'" target="_blank" style="display:block;float:left;margin-right:5px;border:1px solid #fff;width:30px;height:30px;text-align:center;color:#fff;text-decoration:none;background:url('.$url.'/mail_social_sprite.png) top 0px left 0px"></a>';
	}
	if($instagram){
		$instagram = '<a href="'. $instagram.'" target="_blank" style="display:block;float:left;margin-right:5px;border:1px solid #fff;width:30px;height:30px;text-align:center;color:#fff;text-decoration:none;background:url('.$url.'/mail_social_sprite.png) top 0px left -30px;"></a>';
	}
	if($linkedin){
		$linkedin ='<a href="'. $linkedin.'" target="_blank" style="display:block;float:left;margin-right:5px;border:1px solid #fff;width:30px;height:30px;text-align:center;color:#fff;text-decoration:none;background:url('.$url.'/mail_social_sprite.png) top 0px left -60px;"></a>';
	}
	if($twitter){
		$twitter ='<a href="'. $twitter.'" target="_blank" style="display:block;float:left;margin-right:5px;border:1px solid #fff;width:30px;height:30px;text-align:center;color:#fff;text-decoration:none;background:url('.$url.'/mail_social_sprite.png) top 0px left -90px;"></a>';
	}


	$html_mail .= '<!DOCTYPE html>
	<html lang="en">
	<head>
	  <meta charset="utf-8"> <!-- utf-8 works for most cases -->
		<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	  <title>'.$sub.'</title> <!-- the <title> tag shows on email notifications on Android 4.4. -->
	</head>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#f4f4f4" style="margin:20px 0; padding:0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
	<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#f4f4f4" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;"><tr><td>

		<!-- Hidden Preheader Text : BEGIN -->
		<div style="display:none; visibility:hidden; opacity:0; color:transparent; height:0; width:0;line-height:0; overflow:hidden;mso-hide: all;">
			'.$excerpt.'
		</div>
		<!-- Hidden Preheader Text : END -->

	  <!-- Outlook and Lotus Notes dont support max-width but are always on desktop, so we can enforce a wide, fixed width view. -->
	  <!-- Beginning of Outlook-specific wrapper : BEGIN -->
		<!--[if (gte mso 9)|(IE)]>
	  <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
	    <tr>
	      <td>
	  <![endif]-->
	  <!-- Beginning of Outlook-specific wrapper : END -->

	  <!-- Email wrapper : BEGIN -->
	  <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="max-width: 600px;margin: auto;" class="email-container">
	  	<tr>
	      	<td style="text-align:left;padding:20px 40px;font-family:sans-serif;font-size:16px;line-height:24px;color:#fff;background:'.$color.';">
                <img src="'.$tlogo.'" />
            </td>
	    </tr>
	    <tr>
	    	<td>

	        <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff">

	          <!-- Full Width, Fluid Column : BEGIN -->
	          <tr>
	            <td style="padding: 40px; font-family: sans-serif; font-size: 16px; line-height: 24px; color: #666666;">
	              '.$message.'
	            </td>
	          </tr>


				</td>
	      	</tr>
	          <!-- Full Width, Fluid Column : END -->

	        </table>
	    </td>
		</tr>

		<tr>
                <td style="text-align:center;padding:20px 40px;font-family:sans-serif;font-size:12px;line-height:18px;color:#fff;background:#222;border-top:4px solid '.$color.';">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:20px;">
                        <tr>
                            <td style="width:50%;padding:0 10px 0 0;text-align:left;vertical-align:top;">
                                <div style="display:block;width:100%;margin-bottom:15px;">
                                    <img src="'.$blogo.'" style="max-width:150px !important;height:auto;" /><br />
                                </div>

                                '.$facebook.'
                                '.$instagram.'
                                '.$linkedin.'
                                '.$twitter.'

                            </td>
                            <td style="width:50%;padding:0 0 0 10px;text-align:left;color:#fff;">
                                <strong>'.$foretag.'</strong><br />
                                '.$adress.'<br />
                                <a href="mailto:'.$cmail.'" style="width:100%; float:left; display:block;margin-top:5px;color:'.$color.';">'.$cmail.'</a> <br/>
                                <a href="tel:'.str_replace(array(' ','-'),'',$ctelefon).'" style="width:100%; float:left; display:block;margin-top:5px;color:'.$color.';">'.$ctelefon.'</a>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:10px 0;font-family:sans-serif;font-size:12px;line-height:18px;color:#444;background:transparent;">
                	'.$cright.'
                </td>
            </tr>

	  </table>
	  <!-- Email wrapper : END -->

	  <!-- End of Outlook-specific wrapper : BEGIN -->
		<!--[if (gte mso 9)|(IE)]>
	      </td>
	    </tr>
	  </table>
	  <![endif]-->
	  <!-- End of Outlook-specific wrapper : END -->

	</td></tr></table>
	</body>
	</html>';

	return $html_mail;
}

?>
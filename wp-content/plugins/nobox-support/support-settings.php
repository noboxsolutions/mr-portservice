<?php
include 'lib/support-dashboard.php'; // Hämtar innehåll för dashboard
include 'lib/menu/adminbar.php'; // Skapar meny i adminbar
include 'lib/menu/menu.php'; // Skapar meny i sidospalten
include 'lib/support-settings.php'; // Hämtar alla inställningar

// Skapa sida för inställningar
function nobox_support_options_page()
{
    global $support_version;

    $current_user = wp_get_current_user();

    $plugins_url = plugins_url();

    $if_admin = get_option('killer_overlay_admin_username', '');

    $siteUrl = get_site_url();

    $siteUrl = preg_replace('#^https?://#', '', $siteUrl);
?>

<div id="support-settings" class="wrap <?php if (empty($if_admin)) { echo ' first_use'; } else if ($current_user->user_login != $if_admin) { echo ' no_admin'; } ?>">

    <h2 class="main_title">nobox service</h2>

    <div id="tabs">

        <ul class="pluginMenu">

            <li><a href="#tabs-1"><i class="fa fa-cog" aria-hidden="true"></i> Inställningar</a></li>

            <li><a href="#tabs-2"><i class="fa fa-user-secret" aria-hidden="true"></i> Bevakning av webbplats</a></li>

			<li><a href="#tabs-3"><i class="fa fa-bar-chart" aria-hidden="true"></i> Koppling CRM</a></li>

        </ul>

        <form id="settings_form" name="settings_form" method="post" action="options.php">

            <div id="tabs-1">

                <div class="wrapper">

                    <div id="settings_form_content">

                        <?php settings_fields('nobox-settings-group'); ?>



                        <div id="settings_form_div">

                            <div class="left_holder">

                                <h2>Inställningar</h2>

                                <div class="set_section admin">

                                    <b>Uppgifter för administratören</b>

                                    <div class="row username">

                                        <label>Ange användarnamnet från nobox CRM <span class="req"> *</span></label>



                                        <input type="text" class="inputbox admin_username" name="killer_overlay_admin_username" id="admin_username" autocomplete="off" value="<?= get_option('killer_overlay_admin_username', '') ?>" required>  <span id="change_username" title="Ändra användarnamn"><i class="fa fa-refresh"></i></span>

                                    </div>

                                </div>



                                <div class="crm-info">

                                    <input type="hidden" id="site-url" value="<?= $siteUrl ?>">

                                    <button id="fetch-info" class="submit-button f-none">Hämta info från CRM</button>

                                </div>

<br>

                                <div class="set_section saljare personal">

                                    <h3>Kundansvarig</h3>

                                    <div class="row">

                                        <strong>Namn:</strong> <span id="name-seller"><?= get_option('support_namn_saljare', '') ?></span>

                                        <input type="hidden" name="support_namn_saljare" value="<?= get_option('support_namn_saljare', '') ?>">

                                    </div>

                                    <div class="row">

                                        <strong>E-post:</strong> <span id="email-seller"><a href="mailto:<?= get_option('support_email_saljare', '') ?>"><?= get_option('support_email_saljare', '') ?></a></span>

                                        <input type="hidden" name="support_email_saljare" value="<?= get_option('support_email_saljare', '') ?>">

                                    </div>

                                    <div class="row">

                                        <strong>Telefon:</strong> <span id="phone-seller"><a href="tel:<?= get_option('support_telefon_saljare', '') ?>"><?= get_option('support_telefon_saljare', '') ?></a></span>

                                        <input type="hidden" name="support_telefon_saljare" value="<?= get_option('support_telefon_saljare', '') ?>">

                                    </div>

                                   

                                </div>

								

                                <div class="set_section serviceansvarig personal">

                                    <h3>Service och support</h3>

                                   

                                    <div class="row">

                                        <strong>E-post:</strong> <span id="email-service"><a href="mailto:support@nobox.se">support@nobox.se</a></span>

                                        <input type="hidden" name="support_email_serviceansvarig" value="support@nobox.se">

                                    </div>

                                    <div class="row">

                                        <strong>Telefon:</strong> <span id="phone-seller"><a href="tel:0542031818">054-2031818</a></span>

                                        <input type="hidden" name="support_telefon_serviceansvarig" value="054-2031818">

                                    </div>

                                 

                                </div>

                            </div>

                            <div class="right_holder">

                                <h2>Kunden</h2>

                                <div class="set_section kunden">

                                    <b>Uppgifter om kunden</b>

                                    <div class="row checkbox">

                                        <label>Finns serviceavtal?</label>

                                        <?php $serviceavtal_exist = get_option('serviceavtal_exist'); ?>

                                        <input type="checkbox" class="check_avtal" id="serviceavtal_exist" name="serviceavtal_exist[option_kund]" value="1"<?php checked(isset($serviceavtal_exist['option_kund'])); ?> />



                                    </div>

                                    <div class="row select">

                                        <label>Vilket serviceavtal har kunden?</label>



                                        <?php $options = get_option('select_avtal'); ?>

                                        <select class="" name="select_avtal[hide_meta]" id="hide_meta">

                                            <?php $selected = $options['hide_meta']; ?>

                                            <option id="1" name="inget_serviceavtal" value="0" <?php selected($selected, 0); ?>>Inget serviceavtal</option>

                                            <option class="service" id="2" name="serviceavtal_bas" value="1" <?php selected($selected, 1); ?> >Serviceavtal Bas</option>

                                            <option class="service" id="3" name="serviceavtal_flex" value="2" <?php selected($selected, 2); ?>>Serviceavtal Flex</option>

                                            <option class="service" id="4" name="serviceavtal_premium" value="3" <?php selected($selected, 3); ?>>Serviceavtal Premium</option>

                                        </select>

                                    </div>

                                </div>

                            </div>



                            <div class="full_holder">

                                <input type="submit" name="submit" class="submit-button" value="Spara">



                                <a class="rapportera-bugg" href="mailto:support@nobox.se?subject=Jag vill rapportera en bugg i supportpluginet">

                                    <i class="fa fa-exclamation-triangle"></i> Rapportera bugg

                                </a>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div id="tabs-2">

                <div class="wrapper">

                    <div id="plugin_advanced_settings">

                        <?php include_once 'lib/support-coverage.php'; ?>

                    </div>

                </div>

            </div>

        </form>

        <div id="tabs-3">

            <div class="wrapper">

                <div id="crm_connection">

                    <?php include_once 'lib/crm_connection.php'; ?>

                </div>

            </div>

        </div>

    </div>

    <?php if (empty($if_admin)): ?>
        <div id="killer_overlay">
            <div class="inner success">
                <?php if ($current_user->user_firstname): ?>
                    <h2>Hej, <?= $current_user->user_firstname; ?>!</h2>
                <?php else: ?>
                    <h2>Hej, <?= $current_user->user_login; ?>!</h2>
                <?php endif; ?>

                <b>Välkommen till din första användning av pluginet.</b>

                <p>Det första som ska göras nu är att kopiera och klistra in användarnamnet från <b>nobox CRM</b> i det första fältet. Kontrollera även att du har anget förnamn och efternamn på din <a href="profile.php">profil</a>.</p>

                <span id="go-button">Okej, jag förstår.</span>
            </div>
        </div>
    <?php elseif ($current_user->user_login == $if_admin): ?>

    <?php else: ?>
        <div id="killer_overlay">
            <div class="inner failed">

                <?php if ($current_user->user_firstname): ?>

                    <h2>Hej, <?= $current_user->user_firstname; ?>!</h2>

                <?php else: ?>

                    <h2>Hej, <?= $current_user->user_login; ?>!</h2>

                <?php endif; ?>

                <b><p>Ledsen, men du har dessvärre inte behörighet att ändra några inställningarna i vårt plugin.</p></b>

                <p>Har du frågor eller funderingar så är du välkommen att höra av dig till oss, antingen på 054 – 203 1818 eller genom formuläret på <a href="/wp-admin">panelen</a>.</p>

                <a class="go_back" href="/wp-admin">Okej, jag förstår.</a>
            </div>
        </div>

    <?php endif; ?>

</div>

<?php

}


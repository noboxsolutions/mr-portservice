<?php
/**
* Plugin Name: nobox service
* Plugin URI: https://www.nobox.se/
* Description: Pluginet håller koll på uppdateringar av WordPress, plugins och innehåll. Pluginet skickar ut informationsmail med vissa intervaller för att informera om säkerhet och prestanda.
* Version: 3.4.1
* Author: nobox.
* Author URI: http://www.nobox.se/
**/

// Hämta nödvändiga filer
require_once trailingslashit(ABSPATH) . 'wp-admin/includes/plugin.php';

define('PLUGIN_DIR', plugins_url('', __NAMESPACE__ ) . '/nobox-support');

/**
 * Kolla om det finns uppdateringar för pluginet, dessa hämtas från Kernl
 *
 * @link https://kernl.us/documentation
*/

require 'kernl-update-checker/kernl-update-checker.php';

$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://kernl.us/api/v1/updates/5d676b5387f1d26fb4db3f99/',
    __FILE__,
    'nobox-support'
);

// Hämta plugin filer
include_once 'support-settings.php';
include_once 'support-customer.php';
include_once 'support-email-settings.php';

function nobox_update_status_crm()
{
    $siteUrl = get_site_url();
    $siteUrl = preg_replace('#^https?://#', '', $siteUrl);
    $id = get_option('crm_company_id');

    $url = 'https://www.noboxcrm.se/plugin_controller/get_info.php?id='.$id.'&url='.$siteUrl;
    $ch = curl_init($url);

    $opts = array(
        CURLOPT_HEADER => 0,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
    );

    $data = json_encode(array('url' => $siteUrl));

    $opts[CURLOPT_POSTFIELDS] = "data=$data";

    curl_setopt_array($ch, $opts);

    $result = curl_exec($ch);

    curl_close($ch);

    $users = json_decode($result);
    foreach ($users as $user) {
        $role = $user->role;

        $type = '';
        if ($role === 'developer') {
            $type = 'utvecklare';
        } else if ($role === 'service') {
            $type = 'serviceansvarig';
        } else if ($role === 'seller') {
            $type = 'saljare';
        } else if ($role === 'production') {
            $type = 'produktionsledare';
        }

        update_option('support_namn_' . $type, $user->name);
        update_option('support_email_' . $type, $user->email);
        update_option('support_telefon_' . $type, $user->phone);
    }
}

add_action('nobox_cron_hook', 'nobox_update_status_crm');

if (! wp_next_scheduled('nobox_cron_hook')) {
    wp_schedule_event(time(), 'daily', 'nobox_cron_hook');
}


// menu_icon_style_noboxsolutions
add_action('admin_head', function () { ?>
    <style>
        #adminmenu #toplevel_page_noboxsolutions-support-settings .wp-menu-image img { padding: 0 !important; width: 100%; max-width: 20px; height: auto; margin: 8px 0 0  8px; float:left; }
        #adminmenu .current#toplevel_page_noboxsolutions-support-settings .wp-menu-image img { opacity: 1 !important;}
        #wpfooter .clear { display:none; }
    </style>
    <?php
});

add_action('current_screen', function () {
    $currentScreen = get_current_screen();

    if ($currentScreen->id === "toplevel_page_noboxsolutions-support-settings" ||
        $currentScreen->id === "admin_page_noboxsolutions-support-customer") {
        wp_enqueue_script('support-plugin-script-js', PLUGIN_DIR . '/dist/js/script-dist-debug.js?q=1');
        wp_enqueue_style('support-plugin-style-css', PLUGIN_DIR . '/dist/css/style-dist.css');
        wp_enqueue_style('fontawesome-css', PLUGIN_DIR . '/dist/css/fontawesome/font-awesome.min.css');
    }

    if ($currentScreen->id === "dashboard") {
        wp_enqueue_script('support-plugin-dashboard-script-js', PLUGIN_DIR . '/dist/js/script-dashboard-dist.js');
        wp_enqueue_style('support-plugin-dashboard-style-css', PLUGIN_DIR . '/dist/css/dashboard-style-dist.css');
        wp_enqueue_style('fontawesome-css', PLUGIN_DIR . '/dist/css/fontawesome/font-awesome.min.css');
    }
});

// nobox_support_hook
add_action('init', function ($data_array) {
    if (!isset($_REQUEST['nobox-action']) ) {
        return;
    }

    if ($_REQUEST['nobox-action'] === 'get-data') {
        // Hämta url & formatera
        $siteUrl        = get_site_url();
        // $siteUrl        = preg_replace('#^https?://#', '', $siteUrl);
        $siteUrl        = get_site_url();
        $siteUrl        = preg_replace('#^https?://#', '', $siteUrl);
        $siteUrl        = str_replace('&customize_changeset_uuid', '', $siteUrl);
        $siteReturnUrl  = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        // Krytering

        $token = $siteUrl.md5($siteUrl);  // FIXME: Säkerhetsrisk.

        if ($_REQUEST['token'] === $token) {
            nobox_support_getdata();
        }
    }
});

add_action('wp_ajax_nobox_support_fetch_info', function () {
    nobox_update_status_crm();
    wp_die();
});

function nobox_support_getdata()
{
    global $wp_version;

    ini_set('display_errors', true);
    error_reporting(-1);

    require_once ABSPATH . '/wp-includes/post.php';
    require_once ABSPATH . '/wp-admin/includes/update.php';

    $epost_utskick          = get_option('epost_utskick');
    $current_wp_version     = get_option('current_wp_version');
    $plugin_updates         = get_option('plugin_updates');
    $bevaka_posttypes_time  = get_option('bevaka_posttypes_time');
    $wpUpdatesInfo          = get_core_updates();
    $pluginUpdates          = get_plugin_updates();

    // Serviceavtal
    $service_agreement_exist    = get_option('serviceavtal_exist');
    $service_agreement          = get_option('select_avtal');
    if (!empty($service_agreement_exist)) {
        // Serviceavtal? 1=ja 0=nej
        if ($service_agreement_exist['option_kund'] == '1') {
            if ($service_agreement['hide_meta'] == '0') {
                $service_agreement = 'Serviceavtal finns men inget valt i pluginet.';
            } else if ($service_agreement['hide_meta'] == '1') {
                $service_agreement = 'Serviceavtal Bas';
            } else if ($service_agreement['hide_meta'] == '2') {
                $service_agreement = 'Serviceavtal Flex';
            } else if ($service_agreement['hide_meta'] == '3') {
                $service_agreement = 'Serviceavtal Premium';
            } else {
                $service_agreement = 'Serviceavtal: Error';
            }
        } else {
            $service_agreement = 'Inget serviceavtal';
        }
    }

    // Kolla efter uppdateringar
    $pluginCount = count($pluginUpdates);
    $plugin_updates_count = array();
    foreach ($pluginUpdates as $update) {
        $title      = $update->Title;
        $infoUrl    = $update->update->url;
        if (!isset($update->update->tested)) {
            $tested = '-';
        } else {
            $tested     = $update->update->tested;
        }

        $currentVersion         = $update->Version;
        $newVersion         = $update->update->new_version;

        $newVersionLabel = $newVersion;
        if ($currentVersion[0] !== $newVersion[0]) {
            $newVersionLabel .= ' <small style="color:#ff0000;">(Betydande uppdatering)</small>';
        }

        // Skapa array med alla uppdateringar
        $plugin_updates_count[] = array('name' => $title, 'current_version' => $currentVersion, 'new_version' => $newVersionLabel, 'komp_version' => $tested);
    }

    // Kolla efter WP uppdatering
    $wp_update = array();
    foreach ($wpUpdatesInfo as $wpUpdates) {
        $currentWpVersion   = $wpUpdates->current;
        $lang               = $wpUpdates->locale;
        if ($lang == 'sv_SE') {
            if ($wp_version == $currentWpVersion) {
                $wp_update[] = 'Webbplatsen har senaste versionen av WordPress.';
            } else {
                $wp_update[] = 'Webbplatsen har version ' . $wp_version . ' av WordPress och behöver uppdateras till '.$currentWpVersion.'.';
            }
        }
    }

    // Content som ska bevakas
    $cpt_pages  = get_option('types_to_observe');
    $template = get_template_directory();
    include_once $template . "/functions.php";
     $args = array(
       'public'   => true,
       '_builtin' => false,
    );
    $post_types = get_post_types($args, 'objects', 'and');

    $posttypes_to_check = array();
    if (!empty($cpt_pages)) {
        $posttypes_to_check[] = 'page';
    }

    if ($post_types) {
        foreach ($post_types as $post_type) {
            $cpt_name = $post_type->name;

            // Lägg till egna posttypes till arrayen
            $cpt_setting = get_option('cpt_' . $cpt_name);

            if ($cpt_setting) {
                $posttypes_to_check[] = $cpt_name;
            }
        }
    }

    $checkPosts = get_option('bevaka_posttypes_time');
    $dateBeforeModifier = '';
    $dateAfterModifier  = '';
    if ($checkPosts == 'month_1_3') {
        $dateBeforeModifier = "-1 month";
    } else if ($checkPosts == 'month_3_6') {
        $dateBeforeModifier = "-3 month";
    } else if ($checkPosts == 'month_6_1_year') {
        $dateBeforeModifier = "-6 month";
    } else if ($checkPosts == 'year_1_3_year') {
        $dateBeforeModifier = "-1 year";
    }

    $today = date("Y-m-d");

    $dateBefore = date("Y-m-d", strtotime($today . $dateBeforeModifier)) . ' 00:00:00';
    //$dateAfter  = date("Y-m-d", strtotime($today . $dateAfterModifier)) . ' 00:00:00';

    // Hämta från databasen baserat på information ovan.
    global $wpdb;
    $db_prefix      = $wpdb->base_prefix;

    $sql_postype    = implode("' OR post_type = '", $posttypes_to_check);
    $post_query = "SELECT ID, post_title, post_date, post_modified, post_type FROM {$db_prefix}posts WHERE post_type = '$sql_postype' AND post_status = 'publish' AND post_modified < '$dateBefore'";

    //$post_query = "SELECT ID, post_title, post_date, post_modified, post_type FROM {$db_prefix}posts WHERE post_type = '$sql_postype' AND post_status = 'publish' AND post_modified > '$dateAfter' AND post_modified < '$dateBefore'";
    $posts_array    = $wpdb->get_results($post_query, OBJECT);
    $user_receiver = array();
    $args = array(
        'role__in'  => array('editor', 'administrator'),
        'orderby'   => 'nicename',
    );

     $users_to_notify = get_option('users_to_notify');
        $init = false;
        if ($users_to_notify === false) {
            $users_to_notify = array();
            $init = true;
        }

        $user_query = new WP_User_Query($args);
    if (!empty($user_query->results)) {
        foreach ($user_query->results as $user) {
            $username           = $user->user_nicename;
            $user_display_name  = $user->display_name;
            $user_role          = $user->roles[0];
            $user_email         = $user->user_email;
            //$user_setting       = get_option('user_' . $username);

            if (isset($users_to_notify['user_' . $username])) {
                $user_receiver[] = array('display_name' => $user_display_name, 'user_email' => $user_email, 'role' => 'user', 'phone' => '');
            }
        }

        //
        $search  = array('é','É','å','ä','ö','Å','Ä','Ö',' ');
        $replace = array('e','E','','','','','','','');


        $supportSaljareNamn     = get_option('support_namn_saljare');
        $supportSaljareEmail    = get_option('support_email_saljare');
        $supportSaljarePhone    = get_option('support_telefon_saljare');

        if (!empty($supportSaljareNamn)) {
            $fieldName      = strtolower(str_replace($search, $replace, $supportSaljareNamn));
            $user_setting1  = get_option('user_seller_' . $fieldName);

            //if ($user_setting1) {
                if (isset($users_to_notify['seller_' . $fieldName])) {
                $user_receiver[] = array('display_name' => $supportSaljareNamn, 'user_email' => $supportSaljareEmail, 'role' => 'seller', 'user_phone' => $supportSaljarePhone);
            }
        }

    }

    $current_wp_version_text = '';
    if (!empty($current_wp_version['option_wp_version'])) {
        $current_wp_version_text = $current_wp_version['option_wp_version'];
    }

    $plugin_updates_text = '';
    if (!empty($plugin_updates['option_plugin_updates'])) {
        $plugin_updates_text = $plugin_updates['option_plugin_updates'];
    }
    $data_array = array(
        'site_name'                 => get_option('blogname'),
        'service_agreement'         => $service_agreement,
        'site_url'                  => get_site_url(),
        'email_receiver'            => $user_receiver,
        'send_email'                => $epost_utskick,
        'include_wp'                => $current_wp_version_text,
        'wp_update'                 => $wp_update,
        'include_plugin_updates'    => $plugin_updates_text,
        'plugin_updates_count'      => $pluginCount,
        'plugin_updates'            => $plugin_updates_count,
        'check_content'             => get_option('bevaka_posttypes_time'),
        'check_poststypes'          => $posttypes_to_check,
        'check_posts'               => $posts_array,
        'check_posts'               => $posts_array,
    );

    //echo json_encode($data_array, JSON_UNESCAPED_UNICODE);
    $encoded = json_encode($data_array);
    $unescaped = preg_replace_callback('/\\\\u(\w{4})/', function ($matches) {
    return html_entity_decode('&#x' . $matches[1] . ';', ENT_COMPAT, 'UTF-8');
}, $encoded);
    echo $unescaped;
    die();
}

<?php
// Skapa knapp i Admin bar
add_action( 'admin_bar_menu', function ($wp_admin_bar) {
	$args = array(
		'id'      => 'support_customer_page',
		'title'   => 'Support',
		'parent'  => 'top-secondary',
		'href'    => '/wp-admin/admin.php?page=noboxsolutions-support-customer',
		'meta'    => array('class' => 'support-customer-page')
	);
	$wp_admin_bar->add_node($args);
}, 999);

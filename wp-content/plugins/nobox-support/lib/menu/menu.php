<?php
//Skapa menyobjekt
add_action('admin_menu', function () {
    add_action('admin_init', 'register_nobox_settings');

    // Add a new top-level menu (ill-advised):
    add_menu_page(
        __('nobox service','menu-test'),
        __('nobox service','menu-test'),
        'manage_options',
        'noboxsolutions-support-settings',
        'nobox_support_options_page',
        plugins_url('nobox-support/images/icon.png'),
        2
    );
    add_submenu_page(
        'noboxsolutions-support-settings',
        'Inställningar',
        'Inställningar',
        'manage_options',
        'noboxsolutions-support-settings',
        'nobox_support_options_page'
    );
    add_submenu_page(
        null, // 'noboxsolutions-support-settings',
        'Support',
        'Support',
        'manage_options',
        'noboxsolutions-support-customer',
        'support_customer_page'
    );
});
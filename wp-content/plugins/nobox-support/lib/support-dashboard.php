<?php
add_action('current_screen', function () {
    $current_screen = get_current_screen();
    
    if ($current_screen ->id === "dashboard") {
        add_action('admin_notices', function () {
            $data       = get_option('select_avtal');
            $avtal      = "inget_avtal";
            $avtalLabel = 'Inget serviceavtal';
            if ($data['hide_meta'] == 1) {
                $avtal      = 'avtal_bas';
                $avtalLabel = 'Serviceavtal Bas';
            } elseif ($data['hide_meta'] == 2) {
                $avtal      = 'avtal_flex';
                $avtalLabel = 'Serviceavtal Flex';
            } elseif ($data['hide_meta'] == 3) {
                $avtal      = 'avtal_premium';
                $avtalLabel = 'Serviceavtal Premium';
            }
            ?>
            <div class="nobox-wrap">
                <div id="nobox-notice">
                    <div id="nobox-left">
                        <div class="hidden_wrap">
                            <div class="hidden_inner_wrap">
                                <div class="cssload-container">
                                    <div class="cssload-whirlpool"></div>
                                </div>
                            </div>
                        </div>

                        <div class="hidden_content hidden_nobox <?= $avtal; ?>">

                            <h2>Support - <?= $avtalLabel ?></h2>

                            <div class="nobox-info">
                                <?php
                                            $data = get_option('select_avtal');
                                            if ($data['hide_meta'] == 0): ?>
                                <p><b>Välkommen att höra av dig till supporten på Nobox med de frågor du har kring din
                                        webbplats.</b> </p>
                                <p>Eftersom du <u>inte har</u> serviceavtal idag kommer vi att debitera den tid vi lägger ner för
                                    att lösa eventuella problem och vi gör det i mån av tid. Vill du ha prioriterad support
                                    och/eller att vi arbetar löpande med din webbplats, rekommenderar vi att du tecknar ett
                                    serviceavtal.</p>

                                <p>Läs mer om våra <a href="http://www.nobox.se/serviceavtal/" target="_blank"
                                        class="">serviceavtal</a>.</p>
                                <?php elseif ($data['hide_meta'] == 1 || $data['hide_meta'] == 2 || $data['hide_meta'] == 3): ?>
                                <div class="avtal_ingar info-avtal-1">
                                    <p>Nobox avtalskunder har fri support helgfria vardagar mellan 08:00 - 17:00. Servicen gäller
                                        allt som rör webbplats eller webbhotell. </p>

                                    <b>I baspaketet ingår:</b>
                                    <ul>
                                        <li>Prioriterad support vid problem med hemsida, webbhotell m.m.</li>
                                        <li>Månadsvis uppdatering av WordPress och alla plugin</li>
                                        <li>Förslag till förbättringar</li>
                                        <li>Rapportering av vad som utförts</li>
                                    </ul>
                                </div>


                                <?php endif; ?>
                            </div>

                            <div class="nobox-buttons">
                                <a class="support-button" href="admin.php?page=noboxsolutions-support-customer"><i
                                        class="fa fa-envelope-o" aria-hidden="true"></i> Behöver du support?</a>
                            </div>
                        </div>
                    </div>
                    <div id="nobox-right">
                        <div class="hidden_wrap">
                            <div class="hidden_inner_wrap">
                                <div class="cssload-container">
                                    <div class="cssload-whirlpool"></div>
                                </div>
                            </div>
                        </div>
                        <div class="hidden_content hidden_nobox">
                            <div class="nobox_news">
                                <h2>Nyheter från nobox.</h2>
                                <!-- Hämta in nyheter från nobox.se -->
                                <?php
                                            $response = wp_remote_get('https://www.nobox.se/support-plugin/get-news.php');
                                            if (!is_wp_error($response)) {
                                                echo wp_remote_retrieve_body($response);
                                            }
                                            ?>
                            </div>

                            <div class="nobox-logo">
                                <a href="https://nobox.se/" target="_blank" class=""><img class="logotype"
                                        src="<?= plugins_url( '../images/nobox-logotype-black.png', __FILE__ ); ?>"
                                        alt="nobox."></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php
        });
    }
});

<div class="content">
    <h2>Koppling CRM</h2>

    <?php
    // Uppdaterar Option för CRM:et
    $crmConn = false;
    $crmCompanyId = 0;

    if (isset($_GET['successConnect'])) {
        update_option('crm_connection', 'true');
        update_option('crm_company_id', $_GET['id']);
        $crmConn = true;
        $crmCompanyId = $_GET['id'];

    } else if (isset($_GET['alreadyConnected'])) {
        update_option('crm_connection', 'true');
        update_option('crm_company_id', $_GET['id']);
        $crmConn = true;
        $crmCompanyId = $_GET['id'];

    } else if (isset($_GET['resetConnection'])) {
        update_option('crm_connection', 'false');
        update_option('crm_company_id', '');
        $crmConn = false;
        $crmCompanyId = 0;

    } else {
        $crmConn = get_option('crm_connection') == 'true'; // Kollar om koppling är gjord
        $crmCompanyId = get_option('crm_company_id');
    }

    // Hämta url & formatera
    $siteUrl        = get_site_url();
    $siteUrl        = preg_replace('#^https?://#', '', $siteUrl);
    $siteUrl        = str_replace('&customize_changeset_uuid', '', $siteUrl);
    // $siteReturnUrl  = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    $siteReturnUrl = get_admin_url();
    $siteReturnUrl .= 'admin.php?page=noboxsolutions-support-settings';
    $resetUrl = $siteReturnUrl . '&resetConnection';

    // Kryptering
    $token = $siteUrl.md5($siteUrl); // FIXME: Säkerhetsrisk.

    // Utskick
    $epost_utskick = get_option('epost_utskick');

    if (!$crmConn):
        ?>
        <form action="https://www.noboxcrm.se/plugin_controller/plugin.php" method="post">
            <input type="hidden" name="site_url" value="<?= $siteUrl; ?>">
            <input type="hidden" name="site_return_url" value="<?= $siteReturnUrl; ?>">
            <input type="hidden" name="site_token" value="<?= $token; ?>">
            <input type="hidden" name="send_mail" value="<?= $epost_utskick; ?>">

            <input type="submit" name="submit" class="submit-button f-left" value="Anslut till CRM">
        </form>
        <?php 
    else:
        ?>
        <p>Kopplingen till CRM har redan gjorts.</p>
        <a href="<?= $resetUrl ?>" class="submit-button f-left">Ta bort anslutning till CRM</a>
        <?php
    endif;
    ?>
    
    <input type="hidden" name="crm_connection" value="<?= $crmConn ? 'true' : 'false' ?>">
    <input type="hidden" name="crm_company_id" value="<?= $crmCompanyId ?>">
</div>

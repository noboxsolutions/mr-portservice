<?php
// require_once ABSPATH . '/wp-admin/includes/update.php';
$epost_utskick          = get_option('epost_utskick', true);
$current_wp_version     = get_option('current_wp_version');
$plugin_updates         = get_option('plugin_updates');
$bevaka_posttypes_time  = get_option('bevaka_posttypes_time');

// Få uppdateringar på WP och Plugins
global $wp_version;

$wpUpdatesInfo  = get_core_updates();
$pluginUpdates  = get_plugin_updates();
$pluginCount    = count($pluginUpdates);

// Serviceavtal
$service_agreement_exist    = get_option('serviceavtal_exist');
$service_agreement          = get_option('select_avtal');

if (!empty($service_agreement_exist)) {
    // Serviceavtal? 1=ja 0=nej
    if ($service_agreement_exist['option_kund'] == '1') {
        if ($service_agreement['hide_meta'] == '0') {
            $service_agreement = 'Serviceavtal finns men inget valt i pluginet.';

        } elseif ($service_agreement['hide_meta'] == '1') {
            $service_agreement = 'Serviceavtal Bas';

        } elseif ($service_agreement['hide_meta'] == '2') {
            $service_agreement = 'Serviceavtal Flex';

        } elseif ($service_agreement['hide_meta'] == '3') {
            $service_agreement = 'Serviceavtal Premium';

        } else {
            $service_agreement = 'Serviceavtal: Error';
        }
    } else {
        $service_agreement = 'Inget serviceavtal';
    }
}

// Skapa array med all data som ska skickas.
// $data_array = array();
?>

<div class="content">
    <h2>Bevakning av webbplats</h2>

    <div class="row">
        <div class="col">
            <div class="column col_left">
                <h4>Utskick av mail för Plugin &amp; WordPress</h4>
                <b>Påminnelsemail ska gå ut:</b>

                <span class="span_row"><input type="radio" name="epost_utskick" value="send_never" <?php checked('send_never', $epost_utskick, true); ?>>Aldrig</span>
                <span class="span_row"><input type="radio" name="epost_utskick" value="send_every_month" <?php checked('send_every_month', $epost_utskick, true); ?>>Varje månad</span>
                <span class="span_row"><input type="radio" name="epost_utskick" value="send_every_third_month" <?php checked('send_every_third_month', $epost_utskick, true); ?> checked>Var 3:e månad</span>
                <span class="span_row"><input type="radio" name="epost_utskick" value="send_every_sixth_month" <?php checked('send_every_sixth_month', $epost_utskick, true); ?>>Var 6:e månad</span>
            </div>

            <div class="column col_right">
                <b>Inkludera information om:</b>

                <span class="span_row"><input type="checkbox" class="current_wp_version" id="current_wp_version" name="current_wp_version[option_wp_version]" value="1"<?php checked(isset($current_wp_version['option_wp_version'])); ?> checked />Version av WordPress</span>

                <span class="span_row"><input type="checkbox" class="plugin_updates" id="plugin_updates" name="plugin_updates[option_plugin_updates]" value="1"<?php checked(isset($plugin_updates['option_plugin_updates'])); ?> checked />Pluginuppdateringar</span>
            </div>
        </div>

        <div class="col">
            <div class="column col_left">
                <div class="pluginUpdates">
                    <h4>Uppdateringar av plugin (<?= $pluginCount; ?> st)</h4>

                    <ul>
                        <?php
                        $plugin_updates_count = array();

                        foreach ($pluginUpdates as $update):
                            $pluginCurrVer      = $update->Version;
                            $pluginInfoUrl      = $update->update->url;
                            $pluginNewVer       = $update->update->new_version;
                            $pluginTitle        = $update->Title;
                            $pluginVerTest      = '-';

                            if (isset($update->update->tested)) {
                                $pluginVerTest = $update->update->tested;
                            }

                            // Skapa array med alla uppdateringar
                            $plugin_updates_count[] = array('name' => $pluginTitle, 'current_version' => $pluginCurrVer, 'new_version' => $pluginNewVer);
                            ?>
                            <li>
                                <span class="plugin_title"><a href="<?= $pluginInfoUrl; ?>" target="_blank" title="Läs mer om pluginet"><?= $pluginTitle; ?></a></span>
                                <span class="plugin_versions">
                                    <small class="current_version">Nuvarande version: <b class="currVersion"><?= $pluginCurrVer; ?></b></small>
                                    <small class="new_version">Tillgänglig version: <b class="newVersion"><?= $pluginNewVer; ?></b></small>
                                    <small class="tested_wp_version">Kompatibel med WordPress version: <b><?= $pluginVerTest; ?></b></small>
                                </span>
                            </li>
                            <?php
                        endforeach;
                        ?>
                    </ul>
                </div>
            </div>

            <div class="column col_right">

                <div class="WpUpdates">

                    <h4>Uppdatering av WordPress</h4>

                    <ul>

                        <li>Nuvarande version: <b class="currVersion">WordPress <?= $wp_version; ?></b></li>

                        <?php

                        $wp_update = array();

                        foreach ($wpUpdatesInfo as $wpUpdates):

                            if ($wpUpdates->locale !== 'sv_SE') {

                                continue;

                            }



                            $currentWpVersion = $wpUpdates->current;

                            ?>

                            <li>

                                Tillgänglig version:

                                <b class="newVersion">

                                <?php

                                if ($wp_version == $currentWpVersion) {

                                    echo 'Senaste är installerad.';

                                    $wp_update[] = 'Ingen uppdatering finns.';

                                } else {

                                    echo "WordPress $currentWpVersion";

                                    $wp_update[] = 'WordPress ' . $currentWpVersion;

                                }

                                ?>

                                </b>

                            </li>

                        <?php endforeach; ?>

                    </ul>

                </div>

            </div>

        </div>

    </div>

    <div class="row last">

        <div class="col">

            <div class="column col_left">

                <h4>Bevakning av innehåll</h4>

                <b>Kontrollera innehåll som är äldre än:</b>



                <span class="span_row"><input type="radio" name="bevaka_posttypes_time" value="month_1_3" <?php checked('month_1_3', get_option('bevaka_posttypes_time')); ?>>> 1 månad</span>

                <span class="span_row"><input type="radio" name="bevaka_posttypes_time" value="month_3_6" <?php checked('month_3_6', get_option('bevaka_posttypes_time')); ?>>> 3 månader</span>

                <span class="span_row"><input type="radio" name="bevaka_posttypes_time" value="month_6_1_year" <?php checked('month_6_1_year', get_option('bevaka_posttypes_time')); ?>>> 6 månader</span>

                <span class="span_row"><input type="radio" name="bevaka_posttypes_time" value="year_1_3_year" <?php checked('year_1_3_year', get_option('bevaka_posttypes_time')); ?> checked>> 1 år</span>



            </div>

            <div class="column col_right">

                <b>Följande innehåll ska bevakas:</b>



                <?php

                //Hämtar alla posttyper smat posts och pages

                $observes = get_option('types_to_observe');



                if ($observes === false) {

                    $observes = array(

                        'pages' => '1'

                    );

                }



                $args = array(

                   'public'   => true,

                   '_builtin' => false,

                );

                $post_types = get_post_types($args, 'objects', 'and');



                ?>

                <ul class="posttypes_monitor">

                    <li>

                        <div class="check_left">

                            <input type="checkbox" class="cpt_setting" name="types_to_observe[pages]" value="1" <?php checked(isset($observes['pages'])); ?> />

                        </div>

                        <div class="check_right">

                            Sidor

                        </div>

                    </li>



                    <?php

                    // Kollar om bevakning på sidor är markerad

                    $posttypes_to_check = array();

                    if (!empty($cpt_pages)) {

                        $posttypes_to_check[] = 'page';

                    }



                    if ($post_types) {

                        foreach ($post_types as $post_type) {

                            $cpt_name = $post_type->name;

                            $cpt_title = $post_type->label;



                            $isset = isset($observes['cpt_' . $cpt_name]);



                            ?>

                            <li>

                                <div class="check_left">

                                    <input type="checkbox" class="cpt_setting" name="types_to_observe[cpt_<?= $cpt_name ?>]" value="1" <?php checked($isset); ?> />

                                </div>

                                <div class="check_right">

                                    <?= $cpt_title; ?>

                                </div>

                            </li>



                            <?php

                            if ($isset) {

                                $posttypes_to_check[] = $cpt_name;

                            }

                        }

                    }

                    ?>

                </ul>

            </div>

            <?php

            // Kollar vilken intervall på innehåll som ska kollas

            $checkPosts = get_option('bevaka_posttypes_time');

            $dateBeforeModifier = '';

            $dateAfterModifier  = '';

            if ($checkPosts == 'month_1_3') {

                $dateBeforeModifier = "-1 day";

                $dateAfterModifier  = "-4 month";

            } else if ($checkPosts == 'month_3_6') {

                $dateBeforeModifier = "-4 month";

                $dateAfterModifier  = "-7 month";

            } else if ($checkPosts == 'month_6_1_year') {

                $dateBeforeModifier = "-7 month";

                $dateAfterModifier  = "-1 year";

            } else if ($checkPosts == 'year_1_3_year') {

                $dateBeforeModifier = "-1 year";

                $dateAfterModifier  = "-3 year";

            }



            $today = date("Y-m-d");



            $dateBefore = date("Y-m-d", strtotime($today . $dateBeforeModifier)) . ' 00:00:00';

            $dateAfter  = date("Y-m-d", strtotime($today . $dateAfterModifier)) . ' 00:00:00';



            // Hämta från databasen baserat på information ovan.

            global $wpdb;

            $db_prefix      = $wpdb->base_prefix;

            $sql_postype    = implode("' OR post_type = '", $posttypes_to_check);

            $posts_array    = $wpdb->get_results("SELECT ID, post_title, post_date, post_modified, post_type FROM {$db_prefix}posts WHERE post_type = '$sql_postype' AND post_modified > '$dateAfter' AND post_modified < '$dateBefore'", OBJECT);

            ?>

        </div>

        <div class="col">

            <?php



            $search  = array('é', 'É', 'å', 'ä', 'ö', 'Å', 'Ä', 'Ö', ' ');

            $replace = array('e', 'E', '', '', '', '', '', '', '');



            $user_receiver = array();

            ?>

            <div class="column col_full">

                <b>Mottagare på servicemail</b>

                <ul class="user_receiver_list">

                    <?php

                    $users_to_notify = get_option('users_to_notify');

                    $init = false;

                    if ($users_to_notify === false) {

                        $users_to_notify = array();

                        $init = true;

                    }



                    $args = array(

                        'role__in'  => array('editor', 'administrator'),

                        'orderby'   => 'nicename',

                    );

                    $user_query = new WP_User_Query($args);

                    if (!empty($user_query->results)):

                        foreach ($user_query->results as $user):

                            $username           = $user->user_nicename;

                            $user_display_name  = $user->display_name;

                            $user_role          = 'Redaktör';

                            if ($user->roles[0] == 'administrator') {

                                $user_role = 'Administratör';

                            }

                            $user_email = $user->user_email;

							if(!stristr($user_email,'nobox.se') && !stristr($user_email,'noboxsolutions.se'))

							{

								$isChecked  = isset($users_to_notify['user_' . $username]);

								if ($init) {

									$isChecked  = $user_role === 'Administratör';

								}

								?>

								<li>

									<div class="check_left">

										<input type="checkbox" class="user_setting" name="users_to_notify[user_<?= $username ?>]" value="1" <?php checked($isChecked); ?> />

									</div>

									<div class="check_right">

										<b><?= $user_display_name; ?> <span class="userRole">(<?= $user_role; ?>)</span></b>

										<a href="mailto:<?= antispambot($user_email); ?>"><?= antispambot($user_email); ?></a>

									</div>

								</li>

								<?php

								if ($isChecked) {

									$user_receiver[] = array('display_name' => $user_display_name, 'user_email' => $user_email);

								}

							}

                        endforeach;

                    endif;



                    $supportSaljareNamn     = get_option('support_namn_saljare', '');

                    $supportSaljareEmail    = get_option('support_email_saljare', '');

                    if (!empty($supportSaljareNamn) && !empty($supportSaljareEmail)) {

                        $supportSaljareNamnForm = strtolower(str_replace($search, $replace, $supportSaljareNamn));

                        $username_name1 = 'seller_'. $supportSaljareNamnForm;

                        $isChecked      = isset($users_to_notify[$username_name1]);

                        if ($init) {

                            $isChecked = true;

                        }

                        ?>

                        <li>

                            <div class="check_left">

                                <input type="checkbox" class="user_setting" name="users_to_notify[<?= $username_name1 ?>]" value="1" <?php checked($isChecked); ?> />

                            </div>

                            <div class="check_right">

                                <b><?= $supportSaljareNamn; ?> <span class="userRole">(Kundansvarig)</span></b>

                                <a href="<?= antispambot($supportSaljareEmail); ?>"><?= antispambot($supportSaljareEmail); ?></a>

                            </div>

                        </li>

                        <?php

                        if ($isChecked) {

                            $user_receiver[] = array('display_name' => $supportSaljareNamn, 'user_email' => $supportSaljareEmail);

                        }    

                    }

					

					$supportServNamn    = 'Service och support';

                    $supportServEmail   = 'service@nobox.se';

                    if (!empty($supportServNamn) && !empty($supportServEmail)) {

                        $supportServNamnForm = strtolower(str_replace($search, $replace, $supportServNamn));

                        $username_name4 = 'service_' . $supportServNamnForm;

                        $isChecked      = isset($users_to_notify[$username_name4]);

                        if ($init) {

                            $isChecked = false;

                        }

                        ?>

                        <li>

                            <div class="check_left">

                                <input type="checkbox" class="user_setting" name="users_to_notify[<?= $username_name4 ?>]" value="1" <?php checked($isChecked); ?> />

                            </div>

                            <div class="check_right">

                                <b><?= $supportServNamn; ?> <span class="userRole">(Service och support)</span></b>

                                <a href="<?= antispambot($supportServEmail); ?>"><?= antispambot($supportServEmail); ?></a>

                            </div>

                        </li>

                        <?php

                        if ($isChecked) {

                            $user_receiver[] = array('display_name' => $supportServNamn, 'user_email' => $supportServEmail);

                        }

                    }



                    ?>

                </ul>

            </div>

        </div>

    </div>

    <div class="full_holder">

        <input type="submit" name="submit" class="submit-button" value="Spara">

    </div>

</div>

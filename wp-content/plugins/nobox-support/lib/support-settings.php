<?php

// Registera inställningar
function register_nobox_settings()
{
    // CRM Koppling gjord?
    register_setting('nobox-settings-group', 'crm_connection');

    // Spara admin användarnamnet från crmet samt hashkoden
    register_setting('nobox-settings-group', 'killer_overlay_admin_username');

    // Spara om serviceavtal finns
    register_setting('nobox-settings-group', 'serviceavtal_exist');

    // Spara Serviceavtal
    register_setting('nobox-settings-group', 'select_avtal');
    register_setting('nobox-settings-group', 'support_namn_saljare');
    register_setting('nobox-settings-group', 'support_email_saljare');
    register_setting('nobox-settings-group', 'support_telefon_saljare');
    register_setting('nobox-settings-group', 'saljare_get_mail'); //Säljare ska få mail
    register_setting('nobox-settings-group', 'produktionsledare_get_mail'); //Produktionsledare ska få mail
    register_setting('nobox-settings-group', 'utvecklare_get_mail'); //Utvecklare ska få mail
    register_setting('nobox-settings-group', 'serviceansvarig_get_mail'); //Serviceansvarig ska få mail
    register_setting('nobox-settings-group', 'support_namn_produktionsledare');
    register_setting('nobox-settings-group', 'support_email_produktionsledare');
    register_setting('nobox-settings-group', 'support_telefon_produktionsledare');
    register_setting('nobox-settings-group', 'support_namn_utvecklare');
    register_setting('nobox-settings-group', 'support_email_utvecklare');
    register_setting('nobox-settings-group', 'support_telefon_utvecklare');
    register_setting('nobox-settings-group', 'support_namn_serviceansvarig');
    register_setting('nobox-settings-group', 'support_email_serviceansvarig');
    register_setting('nobox-settings-group', 'support_telefon_serviceansvarig');

    // Avancerade inställningar
    register_setting('nobox-settings-group', 'https_checker');
    register_setting('nobox-settings-group', 'epost_utskick');
    register_setting('nobox-settings-group', 'epost_utskick_bevakning');
    register_setting('nobox-settings-group', 'bevaka_posttypes_time');
    register_setting('nobox-settings-group', 'select_bevakning_mottagare'); // Spara mottagare
    register_setting('nobox-settings-group', 'current_wp_version'); // current_wp_version
    register_setting('nobox-settings-group', 'plugin_updates'); // plugin_updates

    // Skapar inställningar för posttypes och custom posttypes_monitor
    register_setting('nobox-settings-group', 'types_to_observe'); // plugin_updates

    // Skapar inställningar för antalet användare som har rollen admin eller redaktör
    register_setting('nobox-settings-group', 'users_to_notify');

    // Skapa inställningar för respektive kontaktperson
    $search  = array('é','É','å','ä','ö','Å','Ä','Ö',' ');
    $replace = array('e','E','','','','','','','');

    $supportSaljareNamn = get_option('support_namn_saljare');

    if ($supportSaljareNamn) {
        $supportSaljareNamnForm = strtolower(str_replace($search, $replace, $supportSaljareNamn));
        register_setting('nobox-settings-group', 'user_seller_' . $supportSaljareNamnForm);
    }

    $supportProduNamn   = get_option('support_namn_produktionsledare');

    if ($supportProduNamn) {
        $supportProduNamnForm = strtolower(str_replace($search, $replace, $supportProduNamn));
        register_setting('nobox-settings-group', 'user_production_' . $supportProduNamnForm);
    }

    $supportDeveNamn    = get_option('support_namn_utvecklare');

    if ($supportDeveNamn) {
        $supportDeveNamnForm = strtolower(str_replace($search, $replace, $supportDeveNamn));
        register_setting('nobox-settings-group', 'user_developer_' . $supportDeveNamnForm);
    }

    $supportServNamn    = get_option('support_namn_serviceansvarig');

    if ($supportServNamn) {
        $supportServNamnForm = strtolower(str_replace($search, $replace, $supportServNamn));
        register_setting('nobox-settings-group', 'user_service_' . $supportServNamnForm);
    }
}

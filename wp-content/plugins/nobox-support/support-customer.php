<?php
function support_customer_page()
{
	$showForm = true;
	$plugins_url = plugins_url();
	$if_admin = get_option('killer_overlay_admin_username');

	global $wp_meta_boxes;

	//Hämta användarinformation för att använda senare.
	$current_user = wp_get_current_user();
	?>

	<div id="customer_support_section">
		<div class="left_content_holder">
			<div class="logotype">
				<a href="" target="_blank">
					<img class="logotype" src="<?= plugins_url('images/nobox-logotype-black.png', __FILE__); ?>" alt="Nobox AB">
				</a>
			</div>

			<div class="info_holder">
				<?php
				$data = get_option('select_avtal');

				if ($data['hide_meta'] == 0):
					?>
					<h2>Inget serviceavtal.</h2>

					<div class="intro">
						<p><b>Välkommen att höra av dig till supporten på nobox med de frågor du har kring din webbplats.</b> </p>
					</div>

					<p>Eftersom du <u>inte har</u> serviceavtal kommer vi att debitera den tid vi lägger ner för att lösa eventuella problem och vi gör det i mån av tid. Vill du ha prioriterad support och/eller att vi arbetar löpande med din webbplats, rekommenderar vi att du tecknar ett serviceavtal.</p>

					<p>Läs mer om våra <a href="https://www.nobox.se/serviceavtal/" target="_blank" class="">serviceavtal</a>.</p>
					<?php
				elseif ($data['hide_meta'] == 1 || $data['hide_meta'] == 2 || $data['hide_meta'] == 3):
					?>
					<h2>Serviceavtal bas.</h2>

					<div class="intro">
						<p><b>Välkommen att höra av dig till supporten på nobox med de frågor du har kring din webbplats.</b></p>
					</div>

					<div class="avtal_ingar">
						<b>I baspaketet ingår:</b>

						<ul>
							<li>Prioriterad support vid problem med hemsida, webbhotell m.m.</li>
							<li>Månadsvis uppdatering av WordPress och alla plugin</li>
							<li>Förslag till förbättringar</li>
							<li>Rapportering av vad som utförts</li>
						</ul>
					</div>
					<?php 
				endif;
				?>
			</div>
		</div>

		<div class="right_content_holder">
			<h2>Vi svarar på dina frågor.</h2>

			<div class="ansvarig_info">
				<div class="group">
					<div class="rad">
						<b><?= get_option('support_namn_saljare'); ?></b>

						<span class="roll">Kundansvarig</span>

						<span class="epost">
							<i class="fa fa-envelope-o" aria-hidden="true"></i>
							<a href="mailto:<?= antispambot(get_option('support_email_saljare'));  ?>"><?= antispambot(get_option('support_email_saljare'));  ?></a>
						</span>

						<span class="tele">
							<i class="fa fa-phone" aria-hidden="true"></i>
							<a href="tel:<?= antispambot(get_option('support_telefon_saljare')); ?>"><?= antispambot(get_option('support_telefon_saljare')); ?></a>
						</span>
					</div>

					<div class="rad">
						<!--<b><?= get_option('support_namn_produktionsledare'); ?></b>-->

						<span class="roll">Service och Support</span>

						<span class="epost">
							<i class="fa fa-envelope-o" aria-hidden="true"></i>
							<a href="mailto:<?= antispambot('support@nobox.se'); ?>"><?= antispambot('support@nobox.se'); ?></a>
						</span>

						<span class="tele">
							<i class="fa fa-phone" aria-hidden="true"></i>
							<a href="tel:<?= antispambot('054-2031818'); ?>"><?= antispambot('054-2031818'); ?></a>
						</span>
					</div>

				</div>
			</div>
		</div>

		<?php
		$showForm = true;

		if (isset($_GET['sent'])):
			$showForm = false;
		?>
			<div id="report-message" class="success">
				<h2>Tack för ditt mail!</h2>

				<p>Vi har mottagit ditt meddelande och återkommer så snart vi kan.</p>
			</div>
		<?php
		elseif (isset($_GET['failed'])):
			?>
			<div id="report-message" class="failed">
				<h2>Ditt mail har inte skickats</h2>

				<p>Det verkar som att något gick snett.</p>

				<p><b>Försök igen</b> eller ring oss på 054 – 203 1818.</p>
			</div>
			<?php 
		endif;
		?>

		<br style="clear:both;" />
	</div>
<?php
}

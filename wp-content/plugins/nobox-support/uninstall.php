<?php
/**
 * https://developer.wordpress.org/plugins/plugin-basics/uninstall-methods/
 */

// Om filen inte anropas av WordPress, stannar den
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

// Alla registrerade inställningar
$option_names = array(
	'crm_connection',
	'killer_overlay_admin_username',
	'serviceavtal_exist',
	'select_avtal',
	'support_namn_saljare',
	'support_email_saljare',
	'support_telefon_saljare',
	'saljare_get_mail',
	'produktionsledare_get_mail',
	'utvecklare_get_mail',
	'serviceansvarig_get_mail',
	'support_namn_produktionsledare',
	'support_email_produktionsledare',
	'support_telefon_produktionsledare',
	'support_namn_utvecklare',
	'support_email_utvecklare',
	'support_telefon_utvecklare',
	'support_namn_serviceansvarig',
	'support_email_serviceansvarig',
	'support_telefon_serviceansvarig',
	'https_checker',
	'epost_utskick',
	'epost_utskick_bevakning',
	'bevaka_posttypes_time',
	'select_bevakning_mottagare',
	'current_wp_version',
	'plugin_updates',
	'types_to_observe',
	'users_to_notify',
);

// Loopa igenom och radera alla registrerade inställningar
foreach ($option_names as $option_name) {
	delete_option($option_name);
}

<?php

// require_once rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/wp-load.php';
// require_once trailingslashit(ABSPATH) . 'wp-includes/pluggable.php';

/****** GLOBAL CONFIGURATION ******/
if (!empty($_SERVER['HTTPS'])) {
    $site_url = "https://" . $_SERVER['SERVER_NAME'];
} else {
    $site_url = "http://" . $_SERVER['SERVER_NAME'];
}

//BOUNDARY (DON'T TOUCH)
$boundary = uniqid('np');

if (isset($_GET['support'])) {
    $fromUrl = $_POST['url'];

    if (stristr($_SERVER['HTTP_REFERER'], $site_url) &&
        noboxservice_smtp_validate($_POST['username']) &&
        noboxservice_smtp_validate($_POST['webbplats']) &&
        noboxservice_smtp_validate($_POST['name']) &&
        noboxservice_smtp_validate($_POST['email'], 'email') &&
        noboxservice_smtp_validate($_POST['telefon']) &&
        noboxservice_smtp_validate($_POST['rubrik']) &&
        noboxservice_smtp_validate($_POST['felmeddelande'])) {

        $username   = $_POST['username'];
        $site       = $_POST['webbplats'];
        $name       = $_POST['name'];
        $email      = $_POST['email'];
        $phone      = $_POST['telefon'];
        $topic      = $_POST['rubrik'];
        $message    = $_POST['felmeddelande'];

        //SET AN ID FOR FORM
        $form_id = 'Support';

        //TITLE IN MAIL
        $sub = $topic . ' | ' . $site;

        //SHORT EXCERPT VIEWED IN EG. GMAIL
        $excerpt = $message;

        //MAIN MESSAGE IN MAIL
        $message = '
        <strong>Skickat av användaren:</strong> '.$username.'<br><br>
        <strong>Namn:</strong> '.$name.'<br>
        <strong>E-post:</strong> '.$email.'<br>
        <strong>Telefon:</strong> '.$phone.'<br>
        <strong style="margin-bottom:10px;">Webbplats:</strong> '.$site.'<br> <br>
        <strong>Ämne:</strong> '.$topic.'<br>
        <strong>Ärende:</strong><br> '.nl2br($message).'<br>
        <br>';

        //SENDERS EMAIL
        $from = $_POST['email'];

        //SENDERS NAME
        $from_name = $_POST['name'];

        $saljare_get_mail = get_option('saljare_get_mail');
        $produktionsledare_get_mail = get_option('produktionsledare_get_mail');
        $utvecklare_get_mail = get_option('utvecklare_get_mail');
        $serviceansvarig_get_mail = get_option('serviceansvarig_get_mail');

        $support_saljare = get_option('support_email_saljare');
        $support_produktionsledare = get_option('support_email_produktionsledare');
        $support_utvecklare = get_option('support_email_utvecklare');
        $support_service_ansvarig = get_option('support_email_serviceansvarig');

        $from = 'epost@epost.se';
        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: '.$name.' <service@nobox.se>', 'Reply-To:'.$email);

        if ($saljare_get_mail) {
            wp_mail($support_saljare, noboxservice_smtp_sub(utf8_encode($sub)), noboxservice_smtp_mail($sub,$message,$excerpt,$fromUrl,$form_id), $headers, noboxservice_smtp_params($from));
        }

        if ($produktionsledare_get_mail) {
            wp_mail($support_produktionsledare, noboxservice_smtp_sub(utf8_encode($sub)), noboxservice_smtp_mail($sub,$message,$excerpt,$fromUrl,$form_id), $headers, noboxservice_smtp_params($from));
        }

        if ($utvecklare_get_mail) {
            wp_mail($support_utvecklare, noboxservice_smtp_sub(utf8_encode($sub)), noboxservice_smtp_mail($sub,$message,$excerpt,$fromUrl,$form_id), $headers, noboxservice_smtp_params($from));
        }

        if ($serviceansvarig_get_mail) {
            wp_mail($support_service_ansvarig, noboxservice_smtp_sub(utf8_encode($sub)), noboxservice_smtp_mail($sub,$message,$excerpt,$fromUrl,$form_id), $headers, noboxservice_smtp_params($from));
        }

        //Kontroll
        wp_mail('benjamin@nobox.se', noboxservice_smtp_sub(utf8_encode($sub)), noboxservice_smtp_mail($sub,$message,$excerpt,$fromUrl,$form_id), $headers, noboxservice_smtp_params($from));

        header("Location: /wp-admin/admin.php?page=noboxsolutions-support-customer&sent");
        exit;

    } else{
        header("Location: /wp-admin/admin.php?page=noboxsolutions-support-customer&failed");
        exit;
    }

}

/************************************* FUNTIONER *************************************/
function noboxservice_smtp_validate($validate,$field_type = null)
{
    return (!empty($validate) && noboxservice_stringHasNoSpam($validate) && noboxservice_field_type_validate($validate,$field_type));
}

function noboxservice_stringHasNoSpam($validate)
{
    $strings_to_search = array('href=', '[/url');
    foreach ($strings_to_search as $needle) {
        if (stristr($validate, $needle)) {
            return false;
        }
    }

    return true;
}

function noboxservice_field_type_validate($validate,$field_type)
{
    if ($field_type === 'email'){
        return filter_var($validate, FILTER_VALIDATE_EMAIL);
    }

    return true;
}

function noboxservice_smtp_sub($sub)
{
    $subject = mb_encode_mimeheader(utf8_decode($sub), 'UTF-8', 'B');
    return $subject;
}

function noboxservice_smtp_params($from)
{
    $params = "-f ".$from;
    return $params;
}

function noboxservice_smtp_mail($sub,$message,$excerpt,$page_sender,$form_id)
{
    $html_mail .= '<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
      <title>'.$sub.'</title> <!-- the <title> tag shows on email notifications on Android 4.4. -->
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#f4f4f4" style="margin:20px 0; padding:0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
    <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#f4f4f4" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;"><tr><td>

        <!-- Hidden Preheader Text : BEGIN -->
        <div style="display:none; visibility:hidden; opacity:0; color:transparent; height:0; width:0;line-height:0; overflow:hidden;mso-hide: all;">
            '.$excerpt.'
        </div>
        <!-- Hidden Preheader Text : END -->

      <!-- Outlook and Lotus Notes dont support max-width but are always on desktop, so we can enforce a wide, fixed width view. -->
      <!-- Beginning of Outlook-specific wrapper : BEGIN -->
        <!--[if (gte mso 9)|(IE)]>
      <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
      <![endif]-->
      <!-- Beginning of Outlook-specific wrapper : END -->

      <!-- Email wrapper : BEGIN -->
      <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="max-width: 600px;margin: auto;" class="email-container">
        <tr>
            <td style="text-align:left;padding:20px 40px;font-family:sans-serif;font-size:16px;line-height:24px;color:#fff;background:#222;">
                <img src="https://www.noboxcrm.se/crm/plugin_controller/nobox-logotype.png" />
            </td>
        </tr>
        <tr>
            <td>

            <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff">

              <!-- Full Width, Fluid Column : BEGIN -->
              <tr>
                <td style="padding: 40px; font-family: sans-serif; font-size: 16px; line-height: 24px; color: #666666;">
                  '.$message.'
                </td>
              </tr>


                </td>
            </tr>
              <!-- Full Width, Fluid Column : END -->

            </table>
        </td>
        </tr>

        <tr>
            <td style="text-align:center;padding:20px 40px;font-family:sans-serif;font-size:12px;line-height:18px;color:#fff;background:#222;border-top:4px solid #222;">
                <table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:20px;">
                    <tr>
                        <td style="width:100%;padding:0 10px 0 0;text-align:left;vertical-align:top;">
                            <div style="display:block;width:100%;text-align:center; font-size:12px;">
                                <span style="float:left; width:100%;">Webbyrån nobox. i Karlstad - En gedigen värmländsk webbyrå</span>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

      </table>
      <!-- Email wrapper : END -->

      <!-- End of Outlook-specific wrapper : BEGIN -->
        <!--[if (gte mso 9)|(IE)]>
          </td>
        </tr>
      </table>
      <![endif]-->
      <!-- End of Outlook-specific wrapper : END -->

    </td></tr></table>
    </body>
    </html>';

    return $html_mail;
}

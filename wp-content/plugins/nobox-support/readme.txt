=== nobox service ===
Requires at least: 4.5
Tested up to: 5.2.2
Requires PHP: 5.6

Pluginet håller koll på uppdateringar av WordPress, plugins och innehåll. Pluginet skickar ut informationsmail med vissa intervaller för att informera om säkerhet och prestanda.

== Changelog ==
3.4.1 Uppdaterat versionsnummer

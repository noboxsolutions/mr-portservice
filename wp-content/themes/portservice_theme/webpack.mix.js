let mix = require("laravel-mix");
let { CleanWebpackPlugin } = require("clean-webpack-plugin");

// AnvÃ¤nd en av dessa
const proxy = "mrport.test";
// const proxy = 'localhost/din-doman';

mix.disableSuccessNotifications();

const publicPath = "./dist";
mix.setPublicPath(publicPath);

const asset = (path) => `assets/${path}`;
const dist = (path) => `${publicPath}/${path}`;

// JavaScript
const jsDist = dist`js`;
mix
  .js(asset`scripts/main.js`, `${jsDist}/main.min.js`)
  .js(asset`scripts/admin/admin.js`, `${jsDist}/admin/admin.min.js`);

// SCSS
const cssDist = dist`css`;
mix
  .sass(asset`styles/main.scss`, `${cssDist}/structure.css`)
  .sass(asset`styles/admin/admin.scss`, `${cssDist}/admin/admin.css`)
  .sass(asset`styles/admin/editor.scss`, `${cssDist}/admin/editor.css`);

mix.copyDirectory(asset`styles/fonts`, dist`fonts`);

if (!mix.inProduction()) {
  mix.browserSync({
    proxy,
    files: ["(lib|templates)/**/*.php", "*.php", dist`(css|js)/*.(css|js)`],
  });
}

mix.webpackConfig({
  externals: {
    jquery: "jQuery",
  },
  plugins: [new CleanWebpackPlugin()],
});

mix.autoload({
  jquery: ["$", "jQuery", "window.jQuery"],
});

mix.options({
  processCssUrls: false,
  postCss: [],
});

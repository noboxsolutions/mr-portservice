<?php if(( $post->post_parent == '7' ) || ( $post->post_parent == '66' ) || ( $post->post_parent == '68' ) || ( $post->post_parent == '74' )) {
	if($post->post_parent == '74') {
		$pageParent = 74;
		$ref_post = 7;
	} elseif ($post->post_parent == '68') {
		$pageParent = 68;
		$ref_post = 7;
	} elseif ($post->post_parent == '66') {
		$pageParent = 66;
		$ref_post = 7;
	} else {
		$pageParent = 7;
		$ref_post = get_the_id();
	}
	$pageId = get_the_id();
?>

	<?php // Om undersidor finns till ovanstående sida (ID) så skriver vi ut en sidebar.
		$noPosts = true;
		$parent_title = get_the_title($ref_post);
		$children = wp_list_pages( array(
			'title_li' 		=> '<h3>' . $parent_title . '</h3> <span class="titleBorder"></span> <div id="toggle_navigation_submenu"><span></span><span></span><span></span><span></span></div>',
			'child_of'		=> $ref_post,
			'echo'			=> '0',
			'sort_column'	=> 'post_title'
		));
		if ($children) {
			echo '<div class="leftSidebar">';
				echo "<ul>$children</ul>";
			echo '</div>';
		} else {
			$noPosts = false;
		}

		$get_post_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'pageCover' );
		$image_url = $get_post_image['0'];
	?>

	<?php if($noPosts == false) { ?>
		<div class="fullContent">
			<?php if($image_url) { ?>
				<div class="left">
					<?php include('templates/page-header-subpage.php'); ?>
					<?php include('templates/page-content.php'); ?>
				</div>
				<div class="right">
					<div class="page-thumbnail">
						<img data-src="<?php echo $image_url ?>" />
					</div>
				</div>
			<?php } else { ?>
				<div class="full">
					<?php include('templates/page-header-subpage.php'); ?>
					<?php include('templates/page-content.php'); ?>
				</div>
			<?php } ?>
		</div>
	<?php } else { ?>
		<div class="rightContent <?php echo $noPosts; ?>">
			<?php include('templates/page-header-subpage.php'); ?>
			<?php include('templates/page-content.php'); ?>
		</div>
	<?php } ?>

<?php } elseif(is_page(13)) { // Kontakt ?>

	<div id="subpageContent">

		<div calss="contactPageTop">
			<?php while (have_posts()) : the_post(); ?>
				<?php get_template_part('templates/page', 'header-subpage'); ?>
			<?php endwhile; ?>
		</div>

		<div class="contactPageLeft">
			<?php if(get_field('personalRep')): ?>
				<ul style="display: flex; flex-wrap: wrap;">
					<?php while(has_sub_field('personalRep')):
						$image 	= get_sub_field('bild');
						$imageUrl 	= $image['sizes']['personalSizes'];
						$namn 	= get_sub_field('namn');
						$tele 	= get_sub_field('telefon');
						$epost 	= get_sub_field('e-post');
					?>
						<li>
							<div class="left">
								<img data-src="<?php echo $imageUrl; ?>" />
							</div>
							<div class="right">
								<span class="name"><?php echo $namn; ?></span>
								<?php if($tele) { ?>
									<span class="tele"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo antispambot($tele); ?>" title="Slå en signal"><?php echo antispambot($tele); ?></a></span>
								<?php } ?>
								<?php if($epost) { ?>
									<span class="epost"><i class="fa fa-envelope-o" aria-hidden="true"></i><a class="titleFix" href="mailto:<?php echo antispambot($epost); ?>" title="Skicka epost"><?php echo antispambot($epost); ?></a></span>
								<?php } ?>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>

		</div>

		<div class="contactPageRight">
			<?php while (have_posts()) : the_post(); ?>
				<?php get_template_part('templates/page', 'content'); ?>
			<?php endwhile; ?>
		</div>

		<div class="contactPageMap">
			<?php $location = get_field('googleMapMarker');

			if( !empty($location) ): ?>
				<div class="acf-map">
					<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
				</div>
			<?php endif; ?>
		</div>
	</div>

<?php } else { ?>

	<?php while (have_posts()) : the_post();
		$get_post_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
		$image_url = $get_post_image['0'];
	?>

		<div id="subpageContent">
			<?php if($image_url) { ?>
				<div class="subpageContentLeft">
					<?php get_template_part('templates/page', 'header-subpage'); ?>
					<?php get_template_part('templates/page', 'content'); ?>
				</div>
				<div class="subpageContentRight">
					<img data-src="<?php echo $image_url; ?>" alt="">
				</div>
			<?php } else { ?>
				<div class="subpageContentFull">
					<?php get_template_part('templates/page', 'header-subpage'); ?>
					<?php get_template_part('templates/page', 'content'); ?>
				</div>
			<?php } ?>

			<?php if(is_page(11)) { // Samarbeten ?>
				<div id="samarbetenLogos">
					<?php if(get_field('samarbetenRep')): ?>
						<ul>
							<?php while(has_sub_field('samarbetenRep')):
								$logo 	= get_sub_field('logotyp');
								$url 	= get_sub_field('lankmal');
							?>
								<li>
									<?php if($url) { ?>
										<a href="<?php echo $url; ?>" target="_blank">
									<?php } ?>
											<img src="<?php echo $logo['sizes']['medium']; ?>">
									<?php if($url) { ?>
										</a>
									<?php } ?>
								</li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			<?php } ?>

			<?php $args = array(
			    'post_type'      => 'page',
			    'posts_per_page' => 3,
			    'post_parent'    => $post->ID,
			    'order'          => 'ASC',
			    'orderby'        => 'menu_order',
			 );
			$parent = new WP_Query( $args );
			if ( $parent->have_posts() ) : ?>
				<div id="childSection">
				    <?php while ( $parent->have_posts() ) : $parent->the_post();
				    	$get_post_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'servicesSize' );
				    	$image_url = $get_post_image['0'];
				    ?>
				        <div class="childBox">
				            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

					            <div class="imageBox">
					           		<div class="cover"></div>
									<img data-src="<?php echo $image_url; ?>">
					            </div>
					            <div class="titleBox">
									<h2><?php the_title(); ?></h2>
					            </div>
				            </a>
				        </div>
				    <?php endwhile; ?>
			    </div>
			<?php endif; wp_reset_query(); ?>

		</div>



	<?php endwhile; ?>
<?php } ?>

<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'lib/assets.php',       // Scripts and stylesheets
    'lib/setup.php',        // Theme setup
    'lib/titles.php',       // Page titles
    'lib/wrapper.php',      // Theme wrapper class
    'lib/customizer.php',   // Theme customizer & Scripts and stylesheets
    'lib/shortcode.php'    // Theme shortcodes
];

foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }
    require_once $filepath;
}
unset($file, $filepath);





/*------------------------------------*\
    PAGINATION
\*------------------------------------*/
add_action('init' , 'pagination');
function pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

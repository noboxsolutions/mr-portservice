<?php
/**
 * Template Name: Startsida
 */
?>
			<div id="frontpageMainSection">
				<?php while (have_posts()) : the_post(); 
					$get_post_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
					$image_url = $get_post_image['0'];
				?>
					<div id="frontpageContentLeft">
						<img data-src="<?php echo $image_url; ?>" alt="">
					</div>
					<div id="frontpageContentRight">
						<?php get_template_part('templates/page', 'header'); ?>
						<?php get_template_part('templates/page', 'content'); ?>
					</div>
				<?php endwhile; ?>
			</div>

			<div id="frontpageTjanstPuff">
				<div id="topContentPuff">
					<?php if(get_field('frontpageTjanster', 'option')): ?>
						<ul class="tjanstNav">
							<?php $i=0; while(has_sub_field('frontpageTjanster', 'option')): 
								$icon 		= get_sub_field('ikon');
								$title 		= get_sub_field('rubrik');
								$text 		= get_sub_field('intro');
							?>
								<li class="navItem" data-navItem="<?php echo $i; ?>"> 
									<div class="left">
										<?php echo $icon ?>
									</div>
									<div class="right">
										<h3><?php echo $title; ?></h3>
										<span><?php echo $text; ?></span>
									</div>
								</li>
							<?php $i++; endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
				<div id="bottomContentPuff">
					<?php if(get_field('frontpageTjanster', 'option')): ?>
						<div id="tjanstSection">
							<?php $i=0; while(has_sub_field('frontpageTjanster', 'option')): 
								$title 		= get_sub_field('rubrik');
								$extraTitle = get_sub_field('introduktionsrubrik');

								$image 		= get_sub_field('bild');
								$image 		= $image['sizes']['frontpageTjanstImage'];
								$content 	= get_sub_field('innehall');
								$urlTarget 	= get_sub_field('lankmal');
							?>
								<div class="tjanstContent" data-content="<?php echo $i; ?>"> 
									
									<div class="right">
										<span><?php echo $extraTitle; ?></span>
										
										<div class="text">
											<h2><?php echo $title; ?></h2>
											<?php echo $content; ?>

											<div class="readMore">
												<a href="<?php echo $urlTarget ?>">Läs mer</a>
											</div>
										</div>
									</div>
									<div class="left">
										<img data-src="<?php echo $image; ?>">
									</div>
								</div>
							<?php $i++; endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div> <!-- // End of .mainBase -->
	</div> <!-- // End of .content -->
</div> <!-- // End of .wrapper wrapper_main -->

<div id="samarbetspartnerSection">
	<div class="wrapper">
		<?php if(get_field('frontpageSamarbeten', 'option')): ?>
			<ul id="partnerSlider" class="owl-carousel owl-theme">
				<?php while(has_sub_field('frontpageSamarbeten', 'option')): 
					$logo = get_sub_field('bild');
					$url = get_sub_field('lankmal');
				?>
					<li class="item">
						<?php if($url) { ?>
							<a href="<?php echo $url; ?>">
						<?php } ?>
							<img src="<?php echo $logo['sizes']['medium']; ?>">
						<?php if($url) { ?>
							</a>
						<?php } ?>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>


<div class="wrapper wrapper_main">
	<div class="content">
		<div class="mainBase">
			<div id="facebookSection">
				<?php 
					$fbSubtitle = get_field('fbSubtitle', 'option');
					$fbTitle = get_field('fbTitle', 'option');
					$fbUrl = get_field('fbUrl', 'option');
					$fbImage = get_field('fbImage', 'option');
				?>
				<div class="left">
					<span class="subTitle"><?php echo $fbSubtitle; ?></span>
					<h2><?php echo $fbTitle; ?></h2>
					
					<div class="fbPosts">
						<?php /* echo do_shortcode('[custom-facebook-feed]'); */ ?>
						<?php echo do_shortcode('[fts_facebook type=page id=287784674967556 posts=2 description=yes posts_displayed=page_only]'); ?>
					</div>

					<?php if($fbUrl) { ?>
						<div class="readMore">
							<a href="<?php echo $fbUrl; ?>" target="_new">
								Hitta oss på Facebook
							</a>
						</div>
					<?php } ?>
				</div>
				<div class="right">
					<img data-src="<?php echo $fbImage['sizes']['large'];?>" />
				</div>

			</div>
<?php use Roots\Sage\Titles; ?>

<div class="page-header">
	<h1><?= Titles\title(); ?></h1>
</div>

<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<div id="breadcrumbs">','</div>');} ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if (has_post_thumbnail()): ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail(''); ?>
			</a>
		<?php endif; ?>

		<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>

		<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>

		<?php the_excerpt(''); ?>

	</article>

<?php endwhile;endif; ?>

<?php include(locate_template('templates/pagination.php')); ?>

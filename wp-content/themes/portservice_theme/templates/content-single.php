<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class('single-post'); ?>>
        <header class="post-header">
            <h1><?php the_title(); ?></h1>
            <div class="meta-info">
                <?php get_template_part('templates/entry-meta'); ?>
            </div>
        </header>
        <div class="post-content">
            <?php the_content(); ?>
        </div>
        <footer class="post-footer">
            
        </footer>
    </article>
<?php endwhile; ?>

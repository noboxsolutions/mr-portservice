<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo get_template_directory_uri(); ?>/favicon.png" rel="shortcut icon">
	<?php wp_head(); ?>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window,document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '807643832724788');

		<?php
			if(is_page(66)) {
				if(isset($_GET['success'])) {
					echo "fbq('track', 'Lead');";
				} else {
					echo "fbq('track', 'PageView');";
				}
			} else {
				echo "fbq('track', 'PageView');";
			}
		?>

	</script>
	<noscript>
	<img height="1" width="1" src="https://www.facebook.com/tr?id=807643832724788&ev=PageView&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->

</head>

<header id="header" class="clear" role="banner">
    <div id="logotype">
        <a href="<?php echo home_url(); ?>">
            <img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/logotype.png" alt="Logotyp för <?php bloginfo('name'); ?>" class="logo-img">
        </a>
    </div>
    
    <div id="toggle_navigation">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>

    <nav id="main_navigation" class="nav">
        <?php wp_nav_menu(['theme_location' => 'mainNav', 'container' => '', 'menu_class' => 'nav_container']); ?>
    </nav>
</header>

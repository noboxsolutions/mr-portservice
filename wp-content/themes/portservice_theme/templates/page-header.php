<?php use Roots\Sage\Titles; 
	// Hämtar Extra rubrikfält
	$extraTitle = get_field('pageOverrubrik');
?>

<div class="page-header">
	<?php if($extraTitle) { ?>
		<span class="pageOverrubrik">
			<?php echo $extraTitle; ?> 
		</span>
	<?php } ?>
	<h1><?= Titles\title(); ?></h1>
</div>

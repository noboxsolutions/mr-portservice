<li <?php post_class('search_post'); ?>>
	<div class="serach-post-header">
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php //if (get_post_type() === 'post') { get_template_part('templates/entry-meta'); } ?>
	</div>
	<div class="serach-post-content">
		<?php the_excerpt(); ?>
	</div>
</li>

<?php if(isset($_GET['success'])) { ?>
    <div class="function_message">
        <div class="inner success">
            <p><i class="fa fa-check" aria-hidden="true"></i> Tack för att du kontaktar oss på MR Portservice AB. Vi återkommer så snart som möjligt!</p>
        </div>
    </div>
    <script>
        fbq('track', 'Lead', {
        value: 10.00,
        currency: 'USD'
        });
    </script>

<?php } ?>

<?php if(isset($_GET['failed'])) { ?>
    <div class="function_message">
        <div class="inner failed">
            <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>  Ditt meddelande gick tyvärr inte att skicka. Var god försök en gång till eller kontakta oss via telefon.</p>
        </div>
    </div>
<?php } ?>

<?php if(isset($_GET['info'])) { ?>
    <div class="function_message">
        <div class="inner info">
            <p><i class="fa fa-info-circle" aria-hidden="true"></i> Exempel på hur ett informationsmeddelande kan se ut.</p>
        </div>
    </div>
<?php } ?>

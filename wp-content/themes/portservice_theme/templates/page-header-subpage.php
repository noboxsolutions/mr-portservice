<?php use Roots\Sage\Titles; 
	// Hämtar Extra rubrikfält
	$extraTitle = get_field('pageOverrubrik');
	if($extraTitle) {
		$extraTitleExist = 'extraTitleExist';
	}

?>
<?php if($noPosts == true) { ?>
	<?php if($image_url) { ?>
		<div class="page-thumbnail">
			<img data-src="<?php echo $image_url ?>" />
		</div>	
	<?php } ?> 
<?php } ?>

<div class="page-header <?php echo $extraTitleExist; ?>">
	<?php if($extraTitle) { ?>
		<span class="pageOverrubrik">
			<?php echo $extraTitle; ?> 
		</span>
	<?php } ?>
	<h1><?= Titles\title(); ?></h1>
	<span class="titleBorder"></span>
</div>

<?php
$footerAdress 	= get_field('footerAdress', 'option');
$footerEpost 	= get_field('footerEpost', 'option');
?>

<div class="wrapper">
	<footer id="footer" role="contentinfo">
		<div class="footerContent">
			<div class="col kontakt">
				<div class="titleBox">
					<h3>Kontakt</h3>
					<span class="titleBorder"></span>
				</div>
				<div class="left">
					<span class="adress"><?php echo $footerAdress; ?></span>
					<span class="epost"><a href="mailto:<?php echo antispambot($footerEpost); ?>" title="Skicka epost"><?php echo antispambot($footerEpost); ?></a></span>

					<div class="socialMedia">
						<?php if (get_field('socialmediaRep', 'option')) : ?>
							<ul>
								<?php while (has_sub_field('socialmediaRep', 'option')) :
									$icon = get_sub_field('ikon');
									$url = get_sub_field('lankmal');
								?>
									<li>
										<a href="<?php echo $url; ?>" target="_blank">
											<?php echo $icon; ?>
										</a>
									</li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>


				</div>
				<div class="right">
					<?php if (get_field('footerTelefonnummer', 'option')) : ?>
						<ul>
							<?php while (has_sub_field('footerTelefonnummer', 'option')) :
								$namn = get_sub_field('namn');
								$tele = get_sub_field('telefonnummer');
							?>
								<li>
									<span><?php echo $namn; ?></span><span><a href="tel:<?php echo antispambot($tele); ?>" title="Slå en signal"><?php echo antispambot($tele); ?></a></span>
								</li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>

				</div>
			</div>

			<div class="col form">
				<div class="titleBox">
					<h3>Snabbkontakt</h3>
					<span class="titleBorder"></span>
				</div>
				<form id="footerForm" class="form clear" name="" method="post" action="/mail/mail-sender.php?contactForm" novalidate="novalidate" autocomplete="false">
					<div class="row left">
						<input type="text" class="inputbox" name="footerName" id="name" placeholder="Namn *">
						<i class="fa fa-check"></i>
						<i class="fa fa-times"></i>
					</div>
					<div class="row right">
						<input type="tel" class="inputbox" name="footerPhone" id="phone" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Telefon *">
						<i class="fa fa-check"></i>
						<i class="fa fa-times"></i>
					</div>
					<div class="row full">
						<input type="email" class="inputbox" name="footerEmail" id="email" placeholder="E-postadress *">
						<i class="fa fa-check"></i>
						<i class="fa fa-times"></i>
					</div>

					<div class="row textarea full">
						<textarea name="footerMessage" id="text" class="textarea" rows="4" placeholder="Meddelande *"></textarea>
						<i class="fa fa-check"></i>
						<i class="fa fa-times"></i>
					</div>

					<div class="spam_validation">
						<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
						<input type="hidden" name="action" value="footer">
					</div>

					<div class="submitRow">
						<input class="submit-button" type="submit" value="Skicka">
					</div>
					<input type="hidden" name="url" value="<?php the_permalink(); ?>" />
				</form>
				<script async defer src="https://www.google.com/recaptcha/api.js?render=<?= GRECAPTCHA_PUBLIC; ?>"></script>
				<script>
					const recaptchaPublicKey = '<?= GRECAPTCHA_PUBLIC; ?>';
				</script>

			</div>
		</div>

		<div class="copyright">
			<span class="copy">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> </span>
			<span class="prod"><?php echo wp_remote_retrieve_body(wp_remote_get('https://noboxcrm.se/producerad.php?domain=mrportservice.se')); ?></span>
		</div>
	</footer>
	<br class="clear" />
</div>

<script>
	/*
setTimeout(function() {
	$("iframe").each(function() {
		$(this).css('display', 'block');
	});
}, 2500);
*/
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
	ga('create', 'UA-90922047-19', 'auto');
	ga('send', 'pageview');
</script>
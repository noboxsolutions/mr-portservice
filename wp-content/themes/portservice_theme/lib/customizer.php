<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

//Lägger Yoast längst ner på sidor och inlägg
add_filter( 'wpseo_metabox_prio', function() { return 'low';});


// Lägger till alla CSS och jQuery filer på rätt ställe
//********************************************************************* 
function assets() {
    wp_enqueue_style('sage/css', Assets\asset_path('../dist/css/structure.css'), false, null);

    //Google API
    wp_register_script('googlemaps','https://maps.googleapis.com/maps/api/js?key=AIzaSyAntm1XUGX8X5GyMv6gy0xQHfKjxOoKT10', array(), '1.0',true); 
    wp_enqueue_script('googlemaps');

    wp_enqueue_script('sage/js', Assets\asset_path('../dist/js/main.min.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
add_action('login_head', __NAMESPACE__ . '\\assets');


// Lägger till CSS och jQuery i backend och logga in sektionen
//********************************************************************* 
function login_style() { // CSS till Loginsidan
    wp_enqueue_style('login_style', Assets\asset_path('../dist/css/admin/admin.css'), false, null);
}
function admin_js_style() { // Scripts i admin
    wp_enqueue_script('sage/customizer', Assets\asset_path('../dist/js/admin/admin.min.js'), ['customize-preview'], null, true);
    wp_enqueue_style('admin_style', Assets\asset_path('../dist/css/admin/admin.css'), false, null);
    add_editor_style(Assets\asset_path('../dist/css/admin/editor.css'));
}
// function fontawesome_dashboard() { // FontAwesome i admin
//    wp_enqueue_style('fontawesome', 'http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css', '', '4.5.0', 'all');
// }
add_action('admin_head', __NAMESPACE__ . '\\admin_js_style');
// add_action('admin_init', __NAMESPACE__ . '\\fontawesome_dashboard');
add_action('login_head', __NAMESPACE__ . '\\login_style');
// add_action('login_head', __NAMESPACE__ . '\\fontawesome_dashboard');
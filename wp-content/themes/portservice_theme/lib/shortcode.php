<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/*------------------------------------*\
    Lägger till Shortcode-funktioner
\*------------------------------------*/
// add_shortcode( 'shortcode_ingess',  __NAMESPACE__ . '\\ingress_shortcode' );
// add_shortcode( 'extern_knapp_lank',  __NAMESPACE__ . '\\extern_knapp_lank' );
// function ingress_shortcode( $atts ) {
//    extract( shortcode_atts(
//        array(
//            'placering' => 'Placering',
//            'ingress' => 'Title',
//        ),
//        $atts
//    ));
//    return '<div class="ingress_text_sc ' . $placering . '">' . $ingress . '</div>';
// }

// function extern_knapp_lank( $atts ) {
//    extract( shortcode_atts(
//        array(
//            'url' => 'Url',
//            'knapptext' => 'Knapptext',
//        ),
//        $atts
//    ));
//    return '<div class="extern_lank_sc"><a href="'.$url.'" title="'.$knapptext.'" target="_blank">' . $knapptext . '</a></div>';
// }

// shortcode_ui_register_for_shortcode(
//     'extern_knapp_lank',
//     array(
//         'label' => 'Knapp med länk (Extern)',
//         'listItemImage'   => 'dashicons-admin-links',
//         'attrs'           => array(
//             array(
//                 'label'        => 'Knapptext',
//                 'attr'         => 'knapptext',
//                 'type'         => 'text',
//                 'description'  => 'Ange namnet på knappen',
//             ),
//             array(
//                 'label'        => 'Url',
//                 'attr'         => 'url',
//                 'type'         => 'text',
//                 'description'  => 'Klistra in länken till det externa länkmålet.',
//             ),
//         ),
//         'post_type'     => array( 'post', 'page' ), 
//     ) 
// );
// shortcode_ui_register_for_shortcode(
//     'shortcode_ingess',
//     array(
//         'label' => 'Lägg till en ingress för posten/sidan',
//         'listItemImage'   => 'dashicons-align-center',
//         'attrs'           => array(
//             array(
//                 'label'        => 'Ingress',
//                 'attr'         => 'ingress',
//                 'type'         => 'textarea',
//                 'description'  => 'Ange en ingresstext som fånger besökarens uppmärksamhet',
//             ),

//             array(
//                 'label'     => 'Placering',
//                 'attr'      => 'placering',
//                 'type'      => 'select',
//                     'options' => array(
//                         'alignleft'      => 'Högerställd',
//                         'aligncenter'    => 'Centrerad',
//                         'alignright'     => 'Vänsterställd',
//                     ),
//                 'description'  => 'Ange om ingressen ska vara högerställd, centrerad eller vänsterställd',
//               ),
//         ),
//         'post_type'     => array( 'post', 'page' ), 
//     )
// );
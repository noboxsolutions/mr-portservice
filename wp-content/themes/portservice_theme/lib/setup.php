<?php
namespace Roots\Sage\Setup;
use Roots\Sage\Assets;

// TEMA INSTÄLLNINGAR
// Registrerar menyer
// Aktiverar Thumbnails
//*********************************************************************
function setup() {
    //Menyer
    //***************************
    register_nav_menus([
        'mainNav' => __('Huvudnavigation', 'sage'),
        'secondNav' => __('Extra navigation', 'sage')
    ]);

    //Thumbnails
    //***************************
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true);
    add_image_size('medium', 250, '', true);
    add_image_size('small', 120, '', true);
    add_image_size('partnerLogo', 250, 100, true);
    add_image_size('personalSizes', 300, '', true);
    add_image_size('pageCover', 1280, 800, true);
    add_image_size('servicesSize', 400, 285, true);
    add_image_size('frontpageTjanstImage', 531, 380, true);
    // add_image_size('custom-size', xxx, '', true); // Custom Thumbnail
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

function body_class($classes) {
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }
    // Add class if sidebar is active
    if (display_sidebar()) {
        $classes[] = 'sidebar-active';
    }
    return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

// Ange vilka sidor sidebaren SKA visas på.
//*********************************************************************
function display_sidebar() {
    static $display;
    isset($display) || $display = in_array(true, [
        //Ange de sidor du vill att sidebaren SKA synas på.
        // @link https://codex.wordpress.org/Conditional_Tags
        // is_404(),
        // is_singular('post_type'),
        // is_page(),
        // is_search(),
        // is_front_page(),
    ]);
    return apply_filters('sage/display_sidebar', $display);
}

// Lägg till en ändelse efter de retunerade orden på the_excerpt
//*********************************************************************
function excerpt_more() { return '...'; }
function excerpt_length_custom( $length ) { return 15; } // Retunerar 15 ord
add_filter( 'excerpt_length', __NAMESPACE__ . '\\excerpt_length_custom', 999 );
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

// Google API
//************************************************************************************
function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyAntm1XUGX8X5GyMv6gy0xQHfKjxOoKT10';
    return $api;
}
add_filter('acf/fields/google_map/api', __NAMESPACE__ . '\\my_acf_google_map_api');


/*------------------------------------*\
    INNITIERA ACF-OPTIONS
\*------------------------------------*/
if( function_exists('acf_add_options_page') ) {
    // lägg till menyalternativ i Admin
    $parent = acf_add_options_page(array(
        'page_title'    => 'Inställningar för webbplats',
        'menu_title'    => 'Funktionalitet',
        'icon_url'      => 'dashicons-admin-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

    // Lägg till undersida till ovanstående
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Submenu',
    //     'menu_title'    => 'Submenu',
    //     'parent_slug'   => $parent['menu_slug'],
    // ));

    acf_add_options_page(array(
        'page_title'    => 'Epost inställningar',
        'menu_title'    => 'Epost inställningar',
        'menu_slug'     => 'custom-email-settings',
        'icon_url'      => 'dashicons-email-alt',
        'post_id'       => 'epost',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}

/*------------------------------------*\
    WIDGETS
\*------------------------------------*/
function widgets_init() {
    // register_sidebar([
    //     'name'          => __('Widget Namn', 'sage'),
    //     'id'            => 'sidebar-primary',
    //     'before_widget' => '<div class="widget %1$s %2$s">',
    //     'after_widget'  => '</div>',
    //     'before_title'  => '<h3>',
    //     'after_title'   => '</h3>'
    // ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/*------------------------------------*\
    CUSTOM POST TYPES
\*------------------------------------*/
// add_action( 'init',__NAMESPACE__ . '\\create_posttype_nobox' );
// function create_posttype_nobox() {
//   register_post_type( 'nobox',
//     array(
//       'labels'                  => array(
//          'name'                 => __('Custom posttype', 'nobox'), // Rename these to suit
//           'singular_name'       => __('Custom postype', 'nobox'),
//           'add_new'             => __('Lägg till', 'nobox'),
//           'add_new_item'        => __('Lägg till', 'nobox'),
//           'edit'                => __('Ändra', 'nobox'),
//           'edit_item'           => __('Ändra', 'nobox'),
//           'new_item'            => __('Nytt', 'nobox'),
//           'view'                => __('Visa', 'nobox'),
//           'view_item'           => __('Visa', 'nobox'),
//           'search_items'        => __('Sök', 'nobox'),
//           'not_found'           => __('Inget funnet', 'nobox'),
//           'not_found_in_trash'  => __('Papperskorgen är tom', 'nobox')
//       ),
//           'public'              => true,
//           'hierarchical'        => false, // Allows your posts to behave like Hierarchy Pages
//           'has_archive'         => true,
//           'menu_icon'           => 'dashicons-randomize',
//           'supports'            => array(
//               'title',
//               'editor',
//               'excerpt',
//               'thumbnail'
//           ), // Go to Dashboard Custom HTML5 Blank post for supports
//           'can_export'          => true, // Allows export in Tools > Export
//           'taxonomies'          => array(
//               //'post_tag',
//               //'category'
//           ) // Add Category and Post Tags support
//       )
//   );
//   flush_rewrite_rules();
// }

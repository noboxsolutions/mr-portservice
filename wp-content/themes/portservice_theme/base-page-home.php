<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
    <?php get_template_part('templates/head'); ?>
    <?php 
        $bodyBg = get_field('bakgrundbild', 'option');
    ?>
    <body <?php body_class(); ?> style='background:url(<?php echo $bodyBg['url']; ?>) no-repeat top left'>
        <!--[if IE]>
            <div class="alert-ie-appeared">
                <?php _e('Du använder <strong>Internet Explorer</strong> som webbläsare. Internet Explorer har från och med januari 2016 slutat få säkerhetsuppdateringar utav Microsoft Corporation. Så för att uppnå den bästa upplevelsen av denna webbplats, var god uppdatera till en annan <a href="http://browsehappy.com/" target="_blank">webbläsare</a>.'); ?>
            </div>
        <![endif]-->
         
         <!-- // Meddelande för olika handlingar -->
        <?php get_template_part('templates/site-messages'); ?>

        <div id="main_wrapper">
            <div class="wrapper wrapper_header">
               <?php do_action('get_header'); get_template_part('templates/header');?>
               <br class="clear">
            </div>

            <div class="wrapper wrapper_main" role="document">
                <div class="content">
                   <div class="mainBase">
                       <?php include Wrapper\template_path(); ?>
                   </div>
                   <?php if (Setup\display_sidebar()) : ?>
                       <div id="sidebar">
                            <?php include Wrapper\sidebar_path(); ?>
                       </div>
                   <?php endif; ?>
                </div>
                <br class="clear">
            </div><!-- /.wrapper -->
            
             <div class="floatFix footer">
                 <?php do_action('get_footer');  get_template_part('templates/footer'); wp_footer(); ?>
             </div>
        </div> <!-- /#main_wrapper -->
    </body>
</html>

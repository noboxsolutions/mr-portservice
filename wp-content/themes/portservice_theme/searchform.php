<form action="<?php echo get_site_url(); ?>" method="get" id="search_form" accept-charset="UTF-8">
	<div class="row_search">
		<input pattern=".{3,}" required title="En sökning bör innehålla minst 3 tecken" type="search" class="search-field" placeholder="<?php if(get_search_query()) { echo sprintf( __( 'Du sökte på '), $wp_query->found_posts ); echo get_search_query(); } else { echo 'Sök...'; } ?>" value="" name="s">
		<input type="submit" class="search-submit" value="Sök">
	</div>						
</form>
<div class="page-header">
	<h1>Error 404 - Sidan finns inte</h1>
	<span class="titleBorder"></span>
</div>

<div class="page-content">
	<p>Sidan du s&#246;ker kan ha blivit borttagen, &#228;ndrat namn eller flyttats. <br/>Var god prova dessa alternativ:</p>
	<ul>
		<li>Kontrollera stavningen</li>
		<li>Kontrollera s&#229; att url:en ser korrekt ut</li>
		<li>G&#229; tillbaka till <a href="<?php echo home_url(); ?>" title="Gå till startsidan">startsidan</a></li>
	</ul>
</div>
import "./jquery/jquery.1.10.2.js";
import "./plugins/cookies/jquery.cookie.js";
import "./plugins/owl-slider/owl.carousel.min.js";
import "./plugins/validate/jquery.validate.js";
import "./main.js";
/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {
  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    //**************************************************************************************************
    // Kör script på startsidan.
    //**************************************************************************************************
    home: {
      init: function () {
        window.recaptchaCallback = function () {
          $("#hiddenRecaptcha").val(1);
          $("#hiddenRecaptcha").valid();
        };

        $(document).ready(function () {
          var n = $("li.item").length;
          if (n > 1) {
            var true_false = true;
          } else {
            var true_false = false;
          }
          $("#partnerSlider").owlCarousel({
            loop: true,
            mouseDrag: true_false,
            nav: true,
            navText: "",
            autoplay: true,
            autoplayTimeout: 10500,
            smartSpeed: 800,
            autoplayHoverPause: true,
            dots: false,
            responsive: {
              0: {
                items: 1,
              },
              600: {
                items: 2,
              },
              1000: {
                items: 4,
              },
            },
          });

          // Visa / Dölj Tjänstepuffar
          //**************************************************************************************************

          $(".navItem[data-navItem=1]").addClass("active");
          $(".tjanstContent[data-content=1]").addClass("active").fadeIn();

          $(".navItem").click(function () {
            $(".navItem").removeClass("active");
            $(".tjanstContent").hide();
            var ID = $(this).attr("data-navItem");

            $(this).addClass("active");

            if ($("[data-content=" + ID + "]").is(":hidden")) {
              $("[data-content=" + ID + "]").fadeIn();
              $("[data-content=" + ID + "]").addClass("active");
            } else {
              $("[data-content=" + ID + "]").fadeOut();
              $("[data-content=" + ID + "]").removeClass("active");
            }
          });
        });
      },
      finalize: function () {
        // JavaScript to be fired on the home page, after the init JS
      },
    },

    //**************************************************************************************************
    // Kör script på ALLA sidor.
    //**************************************************************************************************
    common: {
      init: function () {
        //**************************************************************************************************
        // Alla Script
        //**************************************************************************************************
        $(document).ready(function () {
          setTimeout(function () {
            $(".function_message").slideUp(300);
          }, 5000);

          // Validering
          //**********************************************************************************************
          (function ($, W, D) {
            var JQUERY4U = {};

            JQUERY4U.UTIL = {
              setupFormValidation: function () {
                //Validation rules
                $("#footerForm").validate({
                  ignore: ".ignore",
                  rules: {
                    footerName: {
                      required: true,
                    },
                    footerPhone: {
                      required: true,
                    },
                    footerEmail: {
                      required: true,
                      email: true,
                    },
                    footerMessage: {
                      required: true,
                    },
                  },
                  messages: {
                    footerName: "Ange ditt namn",
                    footerPhone: "Ange ditt telefonnummer",
                    footerEmail: "Ange din e-post",
                    footerMessage: "Skriv ett meddelande",
                    hiddenRecaptcha: "ReCaptcha misslyckades",
                  },
                  submitHandler: function (form) {
                    grecaptcha.ready(() => {
                      grecaptcha
                        .execute(recaptchaPublicKey, {
                          action: "footer",
                        })
                        .then((token) => {
                          document.getElementById(
                            "g-recaptcha-response"
                          ).value = token;
                          form.submit();
                        });
                    });
                  },
                });
              },
            };
            $(D).ready(function ($) {
              JQUERY4U.UTIL.setupFormValidation();
            });
          })(jQuery, window, document);

          (function ($) {
            function new_map($el) {
              var $markers = $el.find(".marker");
              var args = {
                zoom: 12,
                center: new google.maps.LatLng(0, 0),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggable: true,
                mapTypeControl: false,
                zoomControl: false,
                scrollwheel: false,
                streetViewControl: false,
                fullscreenControl: false,

                styles: [
                  {
                    featureType: "poi",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#747474" }, { lightness: "23" }],
                  },
                  {
                    featureType: "poi.attraction",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#f38eb0" }],
                  },
                  {
                    featureType: "poi.government",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#ced7db" }],
                  },
                  {
                    featureType: "poi.medical",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#ffa5a8" }],
                  },
                  {
                    featureType: "poi.park",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#c7e5c8" }],
                  },
                  {
                    featureType: "poi.place_of_worship",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#d6cbc7" }],
                  },
                  {
                    featureType: "poi.school",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#c4c9e8" }],
                  },
                  {
                    featureType: "poi.sports_complex",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#b1eaf1" }],
                  },
                  {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [{ lightness: "100" }],
                  },
                  {
                    featureType: "road",
                    elementType: "labels",
                    stylers: [{ visibility: "off" }, { lightness: "100" }],
                  },
                  {
                    featureType: "road.highway",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#ffd4a5" }],
                  },
                  {
                    featureType: "road.arterial",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#ffe9d2" }],
                  },
                  {
                    featureType: "road.local",
                    elementType: "all",
                    stylers: [{ visibility: "simplified" }],
                  },
                  {
                    featureType: "road.local",
                    elementType: "geometry.fill",
                    stylers: [{ weight: "3.00" }],
                  },
                  {
                    featureType: "road.local",
                    elementType: "geometry.stroke",
                    stylers: [{ weight: "0.30" }],
                  },
                  {
                    featureType: "road.local",
                    elementType: "labels.text",
                    stylers: [{ visibility: "on" }],
                  },
                  {
                    featureType: "road.local",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#747474" }, { lightness: "36" }],
                  },
                  {
                    featureType: "road.local",
                    elementType: "labels.text.stroke",
                    stylers: [{ color: "#e9e5dc" }, { lightness: "30" }],
                  },
                  {
                    featureType: "transit.line",
                    elementType: "geometry",
                    stylers: [{ visibility: "on" }, { lightness: "100" }],
                  },
                  {
                    featureType: "water",
                    elementType: "all",
                    stylers: [{ color: "#d2e7f7" }],
                  },
                ],
              };
              var map = new google.maps.Map($el[0], args);
              map.markers = [];
              $markers.each(function () {
                add_marker($(this), map);
              });

              center_map(map);
              return map;
            }
            function add_marker($marker, map) {
              var latlng = new google.maps.LatLng(
                $marker.attr("data-lat"),
                $marker.attr("data-lng")
              );
              var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: "/mapMarker.png",
              });

              map.markers.push(marker);
              if ($marker.html()) {
                var infowindow = new google.maps.InfoWindow({
                  content: $marker.html(),
                });
                google.maps.event.addListener(marker, "click", function () {
                  infowindow.open(map, marker);
                });
              }
            }

            function center_map(map) {
              var bounds = new google.maps.LatLngBounds();
              $.each(map.markers, function (i, marker) {
                var latlng = new google.maps.LatLng(
                  marker.position.lat(),
                  marker.position.lng()
                );
                bounds.extend(latlng);
              });

              // only 1 marker?
              if (map.markers.length == 1) {
                map.setCenter(bounds.getCenter());
                map.setZoom(12);
              } else {
                map.fitBounds(bounds);
              }
            }

            var map = null;
            $(document).ready(function () {
              $(".acf-map").each(function () {
                map = new_map($(this));
              });
            });
          })(jQuery);

          // Visa dölj menyn (SlideToggle)
          //**********************************************************************************************
          $("#toggle_navigation").click(function () {
            $(this).toggleClass("open");
            $(".nav_container").slideToggle(200);
          });

          $("#toggle_navigation_submenu").click(function () {
            $(this).toggleClass("open");
            $(".leftSidebar > ul > li > ul").slideToggle(200);
          });

          // DÖLJ COOKIE NOTICE SAMT SÄTT KAKAN
          var cookie = $(".coockie_holder");
          $(".acc").click(function () {
            cookie.slideToggle(300);
            $.cookie("cookies", "accepted", { expires: 365, path: "/" });
          });

          $("#menu-huvudnavigation > li.menu-item-has-children").hover(
            function () {
              $(this).children(".sub-menu").addClass("open");
            },
            function () {
              $(this).children(".sub-menu").removeClass("open");
            }
          );
          // Lägg till span.grow på undermenyer för lättare toggle.
          //**********************************************************************************************
          // $('<span class="grow"><i class="fa fa-angle-down"></i></span>').insertAfter('#menu-huvudnavigation li.menu-item-has-children > a');
          // $(".grow").click(function (){
          //     if($(this).html() === '<i class="fa fa-angle-down"></i>') {
          //       $(this).html('<i class="fa fa-angle-down"></i>');
          //       $(this).parent("li").children(".sub-menu").slideToggle(400);
          //     } else {
          //         $(this).html('<i class="fa fa-angle-down"></i></i>');
          //         $(this).parent("li").children(".sub-menu").slideToggle(400);
          //     }
          // });

          // Lägg på classer på meny för att docka den vid scroll.
          //**********************************************************************************************
          var headerHeight = $(".wrapper_header").height();
          $(window).scroll(function () {
            if ($(this).scrollTop() > headerHeight + 80) {
              $(".leftSidebar").addClass("fixed");
            } else {
              $(".leftSidebar").removeClass("fixed");
            }
          });

          $(window).scroll(function () {
            var windowHeight = $(window).height();
            if (windowHeight < 600) {
              if (
                $(window).scrollTop() + $(window).height() ==
                $(document).height()
              ) {
                $(".leftSidebar").fadeOut(300);
              } else {
                $(".leftSidebar").fadeIn(300);
              }
            }
          });

          // Formulär ikoner
          //***********************
          $(".row input, .row select, .row textarea").blur(function () {
            var $this = $(this);
            var parent = $this.parent();
            if ($(this).val().trim().length != 0) {
              $this.addClass("valid");
              $this.removeClass("error");
            } else {
              $this.addClass("error");
              $this.removeClass("valid");
            }
          });

          // Ta bort data-src när bilden är laddad.
          //**********************************************************************************************
          [].forEach.call(
            document.querySelectorAll("img[data-src]"),
            function (img) {
              img.setAttribute("src", img.getAttribute("data-src"));
              img.onload = function () {
                img.removeAttribute("data-src");
              };
            }
          );

          //**********************************************************************************************
          // Placera sidfoten i botten på alla sidor om utrymme uppåt finns.
          //**********************************************************************************************
          var docHeight = $(window).height();
          var footerHeight = $(".floatFix.footer").height();
          var footerTop = $(".floatFix.footer").position().top + footerHeight;
          if (footerTop < docHeight) {
            $(".floatFix.footer").css(
              "margin-top",
              docHeight - footerTop + "px"
            );
          }

          // Spåra Utklick (GA)
          //**********************************************************************************************
          function _gaLt(event) {
            var el = event.srcElement || event.target;
            while (
              el &&
              (typeof el.tagName == "undefined" ||
                el.tagName.toLowerCase() != "a" ||
                !el.href)
            ) {
              el = el.parentNode;
            }
            if (el && el.href) {
              var link = el.href;
              if (
                link.indexOf(location.host) == -1 &&
                !link.match(/^javascript\:/i)
              ) {
                var hitBack = function (link, target) {
                  target
                    ? window.open(link, target)
                    : (window.location.href = link);
                };
                var target =
                  el.target && !el.target.match(/^_(self|parent|top)$/i)
                    ? el.target
                    : false;
                ga(
                  "send",
                  "event",
                  "Outgoing Links",
                  link,
                  document.location.pathname + document.location.search,
                  { hitCallback: hitBack(link, target) }
                );
                event.preventDefault
                  ? event.preventDefault()
                  : (event.returnValue = !1);
              }
            }
          }
          var w = window;
          w.addEventListener
            ? w.addEventListener(
                "load",
                function () {
                  document.body.addEventListener("click", _gaLt, !1);
                },
                !1
              )
            : w.attachEvent &&
              w.attachEvent("onload", function () {
                document.body.attachEvent("onclick", _gaLt);
              });
        }); //End of $(document).ready(function()
      },
      finalize: function () {
        // JavaScript to be fired on all pages, after page specific JS is fired
      },
    },
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function (func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = funcname === undefined ? "init" : funcname;
      fire = func !== "";
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === "function";

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function () {
      // Fire common init JS
      UTIL.fire("common");

      // Fire page-specific init JS, and then finalize JS
      $.each(
        document.body.className.replace(/-/g, "_").split(/\s+/),
        function (i, classnm) {
          UTIL.fire(classnm);
          UTIL.fire(classnm, "finalize");
        }
      );

      // Fire common finalize JS
      UTIL.fire("common", "finalize");
    },
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);
})(jQuery); // Fully reference jQuery after this point.

<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
	<div class="function_message light_message">
		<div class="inner failed">
			<p><?php echo sprintf( __( 'Sökordet '), $wp_query->found_posts ); echo '<span class="term">' . get_search_query() .'</span> gav dessvärre ingen träff.'; ?></p>
		</div>
	</div>
	<div id="search_form">
		<h3>Gör en ny sökning</h3>
		<?php get_search_form(); ?>
	</div>
<?php endif; ?>

<?php if (have_posts()): ?>
	<ul id="search_result">
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('templates/search', 'content'); ?>
		<?php endwhile; ?>
	</ul>
	<div id="search_form">
		<h3>Inte nöjd med resultaten?</h3>
		<?php get_search_form(); ?>
	</div>
<?php endif; ?>
<?php //the_posts_navigation(); ?>